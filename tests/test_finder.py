import pytest

from gp3finder.lib.finder import Finder


def test_finder_files_accepts_string():
    file_path = "FILE_PATH_STRING"
    f = Finder(local_root="SOME_PATH")
    f.files = file_path
    assert file_path in f.files


def test_finder_files_accepts_list_of_strings():
    file_path = ["FILE_PATH_STRING", "FILE_PATH_STRING2"]
    f = Finder(local_root="SOME_PATH")
    f.files = file_path
    assert all(path in f.files for path in file_path)


def test_finder_files_accepts_two_list_of_strings():
    file_path1 = ["FILE_PATH_STRING", "FILE_PATH_STRING2"]
    file_path2 = ["FILE_PATH_STRING3", "FILE_PATH_STRING4"]
    f = Finder(local_root="SOME_PATH")
    f.files = file_path1
    f.files = file_path2
    assert all(path in f.files for path in file_path1 + file_path2)


def test_finder_files_rejects_int():
    file_path = 1234
    f = Finder(local_root="SOME_PATH")
    with pytest.raises(TypeError) as e:
        f.files = file_path
    assert "must be list of strings or a string" in str(e)


def test_finder_files_rejects_list_of_int():
    file_path = [1234, 5678]
    f = Finder(local_root="SOME_PATH")
    with pytest.raises(TypeError) as e:
        f.files = file_path
    assert "must be list of strings or a string" in str(e)


def test_finder_password_files_accepts_string():
    file_path = "FILE_PATH_STRING"
    f = Finder(local_root="SOME_PATH")
    f.password_files = file_path
    assert file_path in f.password_files


def test_finder_password_files_accepts_list_of_strings():
    file_path = ["FILE_PATH_STRING", "FILE_PATH_STRING2"]
    f = Finder(local_root="SOME_PATH")
    f.password_files = file_path
    assert all(path in f.password_files for path in file_path)


def test_finder_password_files_accepts_two_list_of_strings():
    file_path1 = ["FILE_PATH_STRING", "FILE_PATH_STRING2"]
    file_path2 = ["FILE_PATH_STRING3", "FILE_PATH_STRING4"]
    f = Finder(local_root="SOME_PATH")
    f.password_files = file_path1
    f.password_files = file_path2
    assert all(path in f.password_files for path in file_path1 + file_path2)


def test_finder_password_files_rejects_int():
    file_path = 1234
    f = Finder(local_root="SOME_PATH")
    with pytest.raises(TypeError) as e:
        f.password_files = file_path
    assert "must be list of strings or a string" in str(e)


def test_finder_password_files_rejects_list_of_int():
    file_path = [1234, 5678]
    f = Finder(local_root="SOME_PATH")
    with pytest.raises(TypeError) as e:
        f.password_files = file_path
    assert "must be list of strings or a string" in str(e)


def test_finder_cpassword_accepts_cpassword(cpassword):
    f = Finder(local_root="SOME_PATH")
    f.cpasswords = cpassword
    assert cpassword in f.cpasswords


def test_finder_cpassword_accepts_list_of_cpassword(cpassword):
    cpasswords = [cpassword, cpassword]
    f = Finder(local_root="SOME_PATH")
    f.cpasswords = cpasswords
    assert len(f.cpasswords) == len(cpasswords)
    assert sorted(f.cpasswords, key=lambda x: (x.cpassword, x.user, x.file)) == \
           sorted(cpasswords, key=lambda x: (x.cpassword, x.user, x.file))


def test_finder_cpassword_accepts_two_list_of_cpassword(cpassword):
    cpasswords1 = [cpassword, cpassword]
    cpasswords2 = [cpassword, cpassword]
    f = Finder(local_root="SOME_PATH")
    f.cpasswords = cpasswords1
    f.cpasswords = cpasswords2
    cpasswords = cpasswords1 + cpasswords2
    assert len(f.cpasswords) == len(cpasswords)
    assert sorted(f.cpasswords, key=lambda x: (x.cpassword, x.user, x.file)) == \
           sorted(cpasswords, key=lambda x: (x.cpassword, x.user, x.file))


def test_finder_cpassword_rejects_string():
    f = Finder(local_root="SOME_PATH")
    with pytest.raises(TypeError) as e:
        f.cpasswords = "NOT_A_CPASSWORD"
    assert "list of CPassword or a CPassword" in str(e)


def test_finder_cpassword_rejects_list_of_string():
    f = Finder(local_root="SOME_PATH")
    with pytest.raises(TypeError) as e:
        f.cpasswords = ["NOT_A_CPASSWORD", "ALSO_A_CPASSWORD"]
    assert "list of CPassword or a CPassword" in str(e)


def test_local_finder_find_files_all(filesystem):
    f = Finder(local_root=filesystem)
    f._find_files(filesystem)
    assert len(f.files) == 4


def test_local_finder_find_files_xml(filesystem):
    f = Finder(local_root=filesystem)
    f._find_files(filesystem, pattern="*.xml")
    assert len(f.files) == 2


def test_local_finder_process_files(filesystem):
    f = Finder(local_root=filesystem)
    f._find_files(path=filesystem, pattern="*")
    f._process_files(local_path=filesystem, remote_path=filesystem)
    assert len(f.password_files) == 3
    assert len(f.cpasswords) == 2


def test_local_finder_find(filesystem):
    f = Finder(local_root=filesystem)
    f._find(filesystem)
    assert len(f.files) == 2
    assert len(f.password_files) == 2
    assert len(f.cpasswords) == 2


def test_local_finder_run(filesystem):
    f = Finder(local_root=filesystem)
    f.run()
    assert len(f.files) == 2
    assert len(f.password_files) == 2
    assert len(f.cpasswords) == 2
