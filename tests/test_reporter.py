import pytest

from gp3finder.lib.data import CPassword
from gp3finder.lib.reporter import Reporter


@pytest.fixture
def cpasswords():
    cpasswords = [
        CPassword(user_name="administrator",
                  cpassword="LehuaWEr7GE2pOY47SZTTK5EA51GcCyOT9ct0h1d7hQ",
                  file="//dc1/sysvol/some/path/groups.xml"),
        CPassword(
            user_name="dbadmin",
            cpassword=("/NzAelK9zD+fwK74rYBJ7rfyO/nX7Bwc0iyS7NLoJb+MTY1ib+XR"
                       "yvRAPh5MS3vnuD4c3H9YSTXMEzcxzkzVtQ"),
            file="//dc1/sysvol/some/path/services.xml")
    ]
    return cpasswords


@pytest.fixture
def password_files():
    password_files = [
        "//dc1/sysvol/some/path/groups.xml",
        "//dc1/sysvol/some/path/services.xml",
        "//dc1/sysvol/password_backup.xml"
    ]
    return password_files


@pytest.fixture
def outfile(tmp_path):
    dir_ = tmp_path
    file_path = dir_ / "gppfinder.out"
    return str(file_path)


@pytest.fixture
def report(cpasswords, password_files, outfile):
    r = Reporter(cpasswords=cpasswords,
                 password_files=password_files,
                 output_file=outfile)
    expected = """user:password:cpassword:file
administrator:3rKb45rV:LehuaWEr7GE2pOY47SZTTK5EA51GcCyOT9ct0h1d7hQ://dc1/sysvol/some/path/groups.xml
dbadmin:My password ends in spaces!    :/NzAelK9zD+fwK74rYBJ7rfyO/nX7Bwc0iyS7NLoJb+MTY1ib+XRyvRAPh5MS3vnuD4c3H9YSTXMEzcxzkzVtQ://dc1/sysvol/some/path/services.xml
::://dc1/sysvol/some/path/groups.xml
::://dc1/sysvol/some/path/services.xml
::://dc1/sysvol/password_backup.xml
"""
    return r, expected


def test_reporter_cpasswords_returns_list_of_cpassword_instances(report):
    reporter, _ = report
    assert isinstance(reporter.cpasswords, list)
    assert all(
        isinstance(cpassword, CPassword) for cpassword in reporter.cpasswords)


def test_reporter_password_files_returns_list_of_strings(report):
    reporter, _ = report
    assert isinstance(reporter.password_files, list)
    assert all(
        isinstance(filename, str)
        for filename in reporter.password_files)


def test_reporter_output_file_string(report):
    reporter, _ = report
    assert isinstance(reporter.output_file, str)


def test_reporter_output_file_accepts_string(cpasswords, password_files):
    outfile = "OUTPUT_FILE"
    reporter = Reporter(cpasswords=cpasswords,
                        password_files=password_files,
                        output_file=outfile)
    assert isinstance(reporter._output_file, str)


def test_reporter_output_file_rejects_non_string(cpasswords, password_files):
    outfile = 123
    with pytest.raises(Exception) as e:
        _ = Reporter(cpasswords=cpasswords,
                     password_files=password_files,
                     output_file=outfile)
    assert "must be a string" in str(e)


def test_reporter_accepts_list_cpasswords(cpasswords, password_files, outfile):
    reporter = Reporter(cpasswords=cpasswords,
                        password_files=password_files,
                        output_file=outfile)
    assert isinstance(reporter._cpasswords, list)
    assert all(
        isinstance(cpassword, CPassword) for cpassword in reporter._cpasswords)


def test_reporter_rejects_list_non_cpasswords(cpasswords, password_files,
                                              outfile):
    cpasswords.append("Not a CPassword")
    with pytest.raises(TypeError) as e:
        _ = Reporter(cpasswords=cpasswords,
                     password_files=password_files,
                     output_file=outfile)
    assert "must be a list of CPassword" in str(e)


def test_reporter_accepts_list_password_strings(cpasswords, password_files,
                                                outfile):
    reporter = Reporter(cpasswords=cpasswords,
                        password_files=password_files,
                        output_file=outfile)
    assert isinstance(reporter._password_files, list)
    assert all(
        isinstance(file_, str) for file_ in reporter._password_files)


def test_reporter_rejects_list_password_non_strings(cpasswords, password_files,
                                                    outfile):
    password_files.append(1234)
    with pytest.raises(TypeError) as e:
        _ = Reporter(cpasswords=cpasswords,
                     password_files=password_files,
                     output_file=outfile)
    assert "must be a list of strings" in str(e)


def test_reporter_accepts_output_file_string(cpasswords, password_files,
                                             outfile):
    reporter = Reporter(cpasswords=cpasswords,
                        password_files=password_files,
                        output_file=outfile)
    assert isinstance(reporter._output_file, str)


def test_reporter_rejects_output_file_string(cpasswords, password_files):
    outfile = 1234
    with pytest.raises(TypeError) as e:
        _ = Reporter(cpasswords=cpasswords,
                     password_files=password_files,
                     output_file=outfile)
    assert "must be a string" in str(e)


def test_reporter_run(report):
    reporter, expected = report
    reporter.run()
    with open(reporter.output_file, "r") as f:
        assert f.read() == expected
