import pytest
import logging
import docker
import urllib.parse
import time
import tempfile
import os

from contextlib import contextmanager

from gp3finder.lib.data import CPassword


def pytest_configure(config):
    config.addinivalue_line(
        "markers", "requiresmount: mark test which requires mount command."
    )


@contextmanager
def not_raises(exception):
    # https://stackoverflow.com/a/42327075
    try:
        yield
    except exception as e:
        logging.critical(e)
        raise pytest.fail("DID RAISE {0}".format(exception))


@pytest.fixture
def password_file(tmp_path):
    content = "I'm some file with a password in it: SuperSecretPass"
    dir_ = tmp_path / "1"
    dir_.mkdir(exist_ok=True)
    file_path = dir_ / "valid_pass.txt"
    file_path.write_text(content)
    return str(file_path)


@pytest.fixture
def non_password_file(tmp_path):
    content = "I'm just a boring file that should be ignored."
    dir_ = tmp_path / "1"
    dir_.mkdir(exist_ok=True)
    file_path = dir_ / "invalid_pass.txt"
    file_path.write_text(content)
    return str(file_path)


@pytest.fixture
def cpassword():
    cpassword = CPassword(
        cpassword="LehuaWEr7GE2pOY47SZTTK5EA51GcCyOT9ct0h1d7hQ",
        user_name="USERNAME_STRING",
        file="REMOTE_FILE_STRING")
    return cpassword


@pytest.fixture
def groups_file1(tmp_path):
    content = """<?xml version="1.0" encoding="utf-8"?>
 <Groups   clsid="{3125E937-EB16-4b4c-9934-544FC6D24D26}"
           disabled="1">
   <User   clsid="{DF5F1855-51E5-4d24-8B1A-D9BDE98BA1D1}"
           name="DbAdmin"
           image="2"
           changed="2007-07-06 20:45:20"
           uid="{253F4D90-150A-4EFB-BCC8-6E894A9105F7}">
     <Properties
           action="U"
           newName=""
           fullName="Database Admin"
           description="Local Database Admin"
           cpassword="LehuaWEr7GE2pOY47SZTTK5EA51GcCyOT9ct0h1d7hQ"
           changeLogon="0"
           noChange="0"
           neverExpires="0"
           acctDisabled="1"
           userName="DbAdmin"/>
   </User>
 </Groups>"""
    dir_ = tmp_path / "1"
    dir_.mkdir(exist_ok=True)
    file_path = dir_ / "Groups.xml"
    file_path.write_text(content)
    return str(file_path), 1


@pytest.fixture
def groups_file2(tmp_path):
    content = """<?xml version="1.0" encoding="utf-8"?>
 <Groups   clsid="{3125E937-EB16-4b4c-9934-544FC6D24D26}"
           disabled="1">
   <User   clsid="{DF5F1855-51E5-4d24-8B1A-D9BDE98BA1D1}"
           name="DbAdmin"
           image="2"
           changed="2007-07-06 20:45:20"
           uid="{253F4D90-150A-4EFB-BCC8-6E894A9105F7}">
     <Properties
           action="U"
           newName=""
           fullName="Database Admin"
           description="Local Database Admin"
           cpassword="LehuaWEr7GE2pOY47SZTTK5EA51GcCyOT9ct0h1d7hQ"
           changeLogon="0"
           noChange="0"
           neverExpires="0"
           acctDisabled="1"
           userName="DbAdmin"/>
   </User>
   <User   clsid="{DF5F1855-51E5-4d24-8B1A-D9BDE98BA1D1}"
           name="DbAdmin2"
           image="2"
           changed="2007-07-06 20:45:20"
           uid="{253F4D90-150A-4EFB-BCC8-6E894A9105F7}">
     <Properties
           action="U"
           newName=""
           fullName="Database Admin2"
           description="Local Database Admin2"
           cpassword="/NzAelK9zD+fwK74rYBJ7rfyO/nX7Bwc0iyS7NLoJb+MTY1ib+XRyvRAPh5MS3vnuD4c3H9YSTXMEzcxzkzVtQ"
           changeLogon="0"
           noChange="0"
           neverExpires="0"
           acctDisabled="1"
           userName="DbAdmin"/>
   </User>
 </Groups>"""
    # content format from https://docs.microsoft.com/en-us/openspecs/windows_protocols/ms-gppref/37722b69-41dd-4813-8bcd-7a1b4d44a13d
    dir_ = tmp_path / "2"
    dir_.mkdir()
    file_path = dir_ / "Groups.xml"
    file_path.write_text(content)
    return str(file_path), 2


@pytest.fixture
def drives_file1(tmp_path):
    content = """<?xml version="1.0" encoding="utf-8"?>
 <Drives clsid="{8FDDCC1A-0C3C-43cd-A6B4-71A6DF20DA8C}"
         disabled="1">
   <Drive clsid="{935D1B74-9CB8-4e3c-9914-7DD559B7A417}"
          name="S:"
          status="S:"
          image="2"
          changed="2007-07-06 20:57:37"
          uid="{4DA4A7E3-F1D8-4FB1-874F-D2F7D16F7065}">
     <Properties action="U"
                 thisDrive="NOCHANGE"
                 allDrives="NOCHANGE"
                 userName="grim.lab\\administrator"
                 cpassword="LehuaWEr7GE2pOY47SZTTK5EA51GcCyOT9ct0h1d7hQ"
                 path="\\\\scratch"
                 label="SCRATCH"
                 persistent="1"
                 useLetter="1"
                 letter="S"/>
   </Drive>
 </Drives>"""
    # content format from https://docs.microsoft.com/en-us/openspecs/windows_protocols/ms-gppref/1b0df77e-9f78-4be0-b55a-988ac916b425
    dir_ = tmp_path / "1"
    dir_.mkdir(exist_ok=True)
    file_path = dir_ / "Drives.xml"
    file_path.write_text(content)
    return str(file_path), 1


@pytest.fixture
def datasources_file1(tmp_path):
    content = """<?xml version="1.0" encoding="utf-8"?>
 <DataSources clsid="{380F820F-F21B-41ac-A3CC-24D4F80F067B}" disabled="0">
   <DataSource clsid="{5C209626-D820-4d69-8D50-1FACD6214488}" name="SystemNodes"
     image="2" bypassErrors="0" userContext="1" removePolicy="0"
     changed="2007-07-06 20:35:31" uid="{F2174147-A906-4977-AE6F-019C427979D8}">
     <Properties action="U" userDSN="0" dsn="SystemNodes"
       driver="Microsoft Access (*.mdb)" description="All system nodes."
       username="grim.lab\\administrator" cpassword="LehuaWEr7GE2pOY47SZTTK5EA51GcCyOT9ct0h1d7hQ">
       <Attributes>
         <Attribute name="DSN" value="c:\\nodelist.mdb"/>
       </Attributes>
     </Properties>
     <Filters>
       <FilterRunOnce hidden="1" not="0" bool="AND"
         id="{8F7D51B0-F798-4C5F-972B-36FCD0399A33}"/>
     </Filters>
   </DataSource>
 </DataSources>"""
    # content format from https://docs.microsoft.com/en-us/openspecs/windows_protocols/ms-gppref/64ab9fbd-85ae-4457-a2d4-c670fe0ac87b
    dir_ = tmp_path / "1"
    dir_.mkdir(exist_ok=True)
    file_path = dir_ / "DataSources.xml"
    file_path.write_text(content)
    return str(file_path), 1


@pytest.fixture
def printers_file1(tmp_path):
    content = """<?xml version="1.0" encoding="utf-8"?>
 <Printers
           clsid="{1F577D12-3D1B-471e-A1B7-060317597B9C}"
           disabled="1">
   <SharedPrinter
           clsid="{9A5E9697-9095-436d-A0EE-4D128FDFBCE5}"
           name="b35-1053-a" status="b35-1053-a"
           image="2"
           changed="2007-07-06 20:49:50"
           uid="{D954AF72-DDFC-498D-A185-A569A0D02FC4}">
     <Properties
           action="U"
           comment=""
           path="\\\\PRN-CORP1\\b35-1053-a"
           location=""
           default="1"
           skipLocal="1"
           deleteAll="0"
           persistent="0"
           deleteMaps="0"
           port=""
           username="printer_user"
           cpassword="LehuaWEr7GE2pOY47SZTTK5EA51GcCyOT9ct0h1d7hQ"/>
   </SharedPrinter>
 </Printers>"""
    # content format extrapolated from https://docs.microsoft.com/en-us/openspecs/windows_protocols/ms-gppref/821ffb22-1cbc-434d-b64d-709556f059cb and http://www.itrepo.com/recover-passwords-in-microsoft-active-directory-group-policy-object/index.html
    dir_ = tmp_path / "1"
    dir_.mkdir(exist_ok=True)
    file_path = dir_ / "Printers.xml"
    file_path.write_text(content)
    return str(file_path), 1


@pytest.fixture
def scheduledtasks_file1(tmp_path):
    content = """<?xml version="1.0" encoding="utf-8"?>
 <ScheduledTasks clsid="{CC63F200-7309-4ba0-B154-A71CD118DBCC}"
                 disabled="1">
   <Task clsid="{2DEECB1C-261F-4e13-9B21-16FB83BC03BD}"
         name="Cleanup"
         image="2"
         changed="2007-07-06 20:54:40"
         uid="{96C2DBEF-ECAE-4BD4-B1C7-0CD71116595C}">
     <Filters>
       <FilterOs hidden="1"
                 not="1"
                 bool="AND"
                 class="NT"
                 version="VISTA"
                 type="NE"
                 edition="NE"
                 sp="NE"/>
     </Filters>
     <Properties action="U"
                 name="Cleanup"
                 appName="\\\\scratch\\filecleanup.exe"
                 args="-all"
                 startIn="c:\\"
                 comment="Runs for almost 4 hours"
                 enabled="1"
                 deleteWhenDone="0"
                 startOnlyIfIdle="0"
                 stopOnIdleEnd="0"
                 noStartIfOnBatteries="1"
                 stopIfGoingOnBatteries="1"
                 systemRequired="0"
                 runAs="task_user"
                 cpassword="LehuaWEr7GE2pOY47SZTTK5EA51GcCyOT9ct0h1d7hQ">
       <Triggers>
         <Trigger type="DAILY"
                  startHour="10"
                  startMinutes="0"
                  beginYear="2007"
                  beginMonth="7"
                  beginDay="6"
                  hasEndDate="0"
                  repeatTask="0"
                  interval="1"/>
       </Triggers>
     </Properties>
   </Task>
 </ScheduledTasks>"""
    # content format extrapolated from https://docs.microsoft.com/en-us/openspecs/windows_protocols/ms-gppref/850b333b-9336-496a-bf93-a20f33748454 and http://www.itrepo.com/recover-passwords-in-microsoft-active-directory-group-policy-object/index.html
    dir_ = tmp_path / "1"
    dir_.mkdir(exist_ok=True)
    file_path = dir_ / "Scheduledtasks.xml"
    file_path.write_text(content)
    return str(file_path), 1


@pytest.fixture
def services_file1(tmp_path):
    content = """<?xml version="1.0" encoding="utf-8"?>
 <NTServices
           clsid="{2CFB484A-4E96-4b5d-A0B6-093D2F91E6AE}">
   <NTService
           clsid="{AB6F0B67-341F-4e51-92F9-005FBFBA1A43}"
           name="Computer Browser"
           image="0"
           changed="2007-07-10 22:52:45"
           uid="{8A3CC7D5-89F1-44DB-8D41-80F6471E17BF}">
     <Properties
           startupType="NOCHANGE"
           serviceName="Computer Browser"
           timeout="30"
           accountName="LocalSystem"
           interact="1"
           firstFailure="NOACTION"
           secondFailure="NOACTION"
           thirdFailure="RESTART"
           resetFailCountDelay="0"
           restartServiceDelay="900000"
           cpassword="LehuaWEr7GE2pOY47SZTTK5EA51GcCyOT9ct0h1d7hQ"/>
   </NTService>
 </NTServices>
  """
    # content format extrapolated from https://docs.microsoft.com/en-us/openspecs/windows_protocols/ms-gppref/c4561a5c-f2fc-4a24-bffc-700d2e00403d and http://www.itrepo.com/recover-passwords-in-microsoft-active-directory-group-policy-object/index.html
    dir_ = tmp_path / "1"
    dir_.mkdir(exist_ok=True)
    file_path = dir_ / "Services.xml"
    file_path.write_text(content)
    return str(file_path), 1


@pytest.fixture(params=[
    "groups_file1", "groups_file2", "services_file1", "scheduledtasks_file1",
    "datasources_file1", "printers_file1", "drives_file1"
])
def gppfile(request):
    return request.getfixturevalue(request.param)


@pytest.fixture
def filesystem(tmp_path, password_file, non_password_file, groups_file1,
               drives_file1):
    return str(tmp_path)


def create_samba_docker_context():
    # Reduced the default HEALTCHECK interval to speed up tests.
    dockerfile_content = """FROM dperson/samba
WORKDIR /srv
COPY Groups.xml .
HEALTHCHECK --interval=5s --timeout=15s \
            CMD smbclient -L \\localhost -U % -m SMB3
"""
    groups_content = """<?xml version="1.0" encoding="utf-8"?>
 <Groups   clsid="{3125E937-EB16-4b4c-9934-544FC6D24D26}"
           disabled="1">
   <User   clsid="{DF5F1855-51E5-4d24-8B1A-D9BDE98BA1D1}"
           name="DbAdmin"
           image="2"
           changed="2007-07-06 20:45:20"
           uid="{253F4D90-150A-4EFB-BCC8-6E894A9105F7}">
     <Properties
           action="U"
           newName=""
           fullName="Database Admin"
           description="Local Database Admin"
           cpassword="LehuaWEr7GE2pOY47SZTTK5EA51GcCyOT9ct0h1d7hQ"
           changeLogon="0"
           noChange="0"
           neverExpires="0"
           acctDisabled="1"
           userName="DbAdmin"/>
   </User>
 </Groups>"""

    # Note can't use tmp_path from pytest here since smbshare
    # is scoped to session (or module) which is incompatible
    # with tmp_path.
    dir_ = tempfile.mkdtemp()
    dockerfile_path = os.path.join(dir_, "Dockerfile")
    with open(dockerfile_path, "w") as f:
        f.write(dockerfile_content)
    groups_path = os.path.join(dir_, "Groups.xml")
    with open(groups_path, "w") as f:
        f.write(groups_content)
    return str(dir_), str(dockerfile_path)


@pytest.fixture(scope="session")
def smbshare():
    user = "someuser"
    password = "somepassword"
    domain = "GRIMLAB"
    share = "TESTSYSVOL"

    # TODO: need to enable IPC$ share on SAMBA.
    # Currently the auth test fails because we do not have an IPC$
    # share with the correct share type.
    # TODO: ideally should use SYSVOL as the sharename as that is real world
    # however Windows errors connecting to a SAMBA share called SYSVOL and I
    # can't figure out why.
    environment = {
        # 'SHARE': '{} private share;/someuser;no;no;no;someuser'.format(share),
        # 'SHARE': '{};/srv;yes;yes;no'.format(share),
        'SHARE': '{};/srv;no;no;no;{}'.format(share, user),
        # 'SHARE2': 'ipc$;/share',
        'USER': ';'.join([user, password]),
        'PERMISSIONS': True,
        'WORKGROUP': domain,
        'NMBD': True,
        }

    ports = {
        '445/tcp': '445',
        '139/tcp': '139',
        '137/udp': '137',
        '138/udp': '138',
    }

    # Explicitly passing environment since we get the host from here as well.
    docker_env = os.environ
    c_name = "gp3finder_tests_samba"
    labels = [
        'testcase',
        'gp3finder'
    ]

    client = docker.from_env(environment=docker_env)
    samba_image_name = "gp3finder_samba"
    samba_docker_path, samba_docker_file = create_samba_docker_context()
    client.images.build(path=samba_docker_path,
                        dockerfile=samba_docker_file,
                        rm=True, pull=True, tag=samba_image_name)
    container = client.containers.run(samba_image_name, environment=environment,
                                      ports=ports, detach=True, labels=labels,
                                      name=c_name, auto_remove=True, remove=True)

    container.reload()
    while not container.attrs["State"]["Health"]["Status"] == "healthy":
        logging.debug("SAMBA container not healthy, giving it a second...")
        time.sleep(1)  # Give the container a second to get going...
        # ```local attributes are cached; users may call reload() to
        # query the Docker daemon for the current properties,
        # causing attrs to be refreshed.````
        # - https://docker-py.readthedocs.io/en/stable/containers.html#container-objects
        container.reload()
        # Once the container is up and running we are ready to do our tests
        # Also we should be able to get the IPAddress from the attrs.

    # If DOCKER_HOST is specified then we assume we can access the SAMBA
    # container on that IP (probably running tests outside container.)
    # If DOCKER_HOST is not set, assume we are in a container.
    # If we appear to be conencted ot the default bridge network, get the
    # IP address of the SAMBA container.
    # If we do not appear to be connected to the default bridge, use
    # the container name to take advantage of the DNS capabilities of
    # a user specified network in Docker.
    # TODO:
    # Making some big assumptions here and its a bit fragile/complex...
    samba_host = None
    if docker_env.get("DOCKER_HOST"):
        logging.debug("setting samba_host from DOCKER_HOST")
        samba_host = urllib.parse.urlparse(docker_env.get("DOCKER_HOST")).hostname
    else:
        try:
            # logging.debug(pprint.pprint(container.attrs["NetworkSettings"]["Networks"]["bridge"]))
            samba_host = container.attrs["NetworkSettings"]["Networks"]["bridge"]["IPAddress"]
            logging.debug("setting samba_host from bridge network IP")
        except KeyError:
            logging.debug("Failed to get bridge IP. Assuming we are not on user defined network. Setting samba_host from container name.")

            samba_host = c_name

    if not samba_host:
        raise EnvironmentError("Could find samba host name...")

    creds = {
        "user": user,
        "passwd": password,
        "domain": domain,
        "share": share,
        "host": samba_host
    }
    yield creds
    container.stop()
