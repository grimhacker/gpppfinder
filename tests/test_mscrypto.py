import pytest

from gp3finder.lib.mscrypto import MSCrypto


def test_iv():
    expected_iv = b'\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0'
    crypto = MSCrypto()
    assert crypto.iv == expected_iv


def test_key():
    expected_key = (b"\x4e\x99\x06\xe8\xfc\xb6\x6c\xc9\xfa\xf4\x93\x10\x62\x0f"
                    b"\xfe\xe8\xf4\x96\xe8\x06\xcc\x05\x79\x90\x20\x9b\x09\xa4"
                    b"\x33\xb6\x6c\x1b")
    crypto = MSCrypto()
    assert crypto.key == expected_key


def test_encrypt_plaintext():
    plaintext = "3rKb45rV"
    expected_ciphertext = (b"\x2d\xe8\x6e\x69\x61\x2b\xec\x61\x36\xa4\xe6\x38"
                           b"\xed\x26\x53\x4c\xae\x44\x03\x9d\x46\x70\x2c\x8e"
                           b"\x4f\xd7\x2d\xd2\x1d\x5d\xee\x14")
    crypto = MSCrypto()
    ciphertext = crypto._encrypt_plaintext(plaintext)
    assert ciphertext == expected_ciphertext


def test_encode_decoded_ciphertext():
    ciphertext = (b"\x2d\xe8\x6e\x69\x61\x2b\xec\x61\x36\xa4\xe6\x38\xed\x26"
                  b"\x53\x4c\xae\x44\x03\x9d\x46\x70\x2c\x8e\x4f\xd7\x2d\xd2"
                  b"\x1d\x5d\xee\x14")
    expected_encoded_ciphertext = "LehuaWEr7GE2pOY47SZTTK5EA51GcCyOT9ct0h1d7hQ"
    crypto = MSCrypto()
    encoded_ciphertext = crypto._encode_ciphertext(ciphertext)
    assert encoded_ciphertext == expected_encoded_ciphertext


def test_encrypt_1():
    # password known to cause an error if convert from utf8 to utf16
    # rather than to utf16le
    plaintext = "3rKb45rV"
    crypto = MSCrypto()
    _, ciphertext = crypto.encrypt(plaintext)
    assert ciphertext == "LehuaWEr7GE2pOY47SZTTK5EA51GcCyOT9ct0h1d7hQ"


def test_encrypt_2():
    plaintext = "My password ends in spaces!    "
    crypto = MSCrypto()
    _, ciphertext = crypto.encrypt(plaintext)
    assert ciphertext == ("/NzAelK9zD+fwK74rYBJ7rfyO/nX7Bwc0iyS7NLoJb+MTY1ib"
                          "+XRyvRAPh5MS3vnuD4c3H9YSTXMEzcxzkzVtQ")


def test_decode_cpassword():
    encoded_ciphertext = "LehuaWEr7GE2pOY47SZTTK5EA51GcCyOT9ct0h1d7hQ"
    expected_decoded_ciphertext = (b"\x2d\xe8\x6e\x69\x61\x2b\xec\x61\x36\xa4"
                                   b"\xe6\x38\xed\x26\x53\x4c\xae\x44\x03\x9d"
                                   b"\x46\x70\x2c\x8e\x4f\xd7\x2d\xd2\x1d\x5d"
                                   b"\xee\x14")
    crypto = MSCrypto()
    decoded_ciphertext = crypto._decode_cpassword(encoded_ciphertext)
    assert decoded_ciphertext == expected_decoded_ciphertext


def test_decrypt_decoded_ciphertext():
    decoded_ciphertext = (b"\x2d\xe8\x6e\x69\x61\x2b\xec\x61\x36\xa4\xe6\x38"
                          b"\xed\x26\x53\x4c\xae\x44\x03\x9d\x46\x70\x2c\x8e"
                          b"\x4f\xd7\x2d\xd2\x1d\x5d\xee\x14")
    expected_plaintext = "3rKb45rV"
    crypto = MSCrypto()
    plaintext = crypto._decrypt(decoded_ciphertext)
    assert plaintext == expected_plaintext


def test_decrypt_1():
    ciphertext = "LehuaWEr7GE2pOY47SZTTK5EA51GcCyOT9ct0h1d7hQ"
    crypto = MSCrypto()
    plaintext = crypto.decrypt(ciphertext)
    assert plaintext == "3rKb45rV"


def test_decrypt_2():
    ciphertext = ("/NzAelK9zD+fwK74rYBJ7rfyO/nX7Bwc0iyS7NLoJb+MTY1ib+"
                  "XRyvRAPh5MS3vnuD4c3H9YSTXMEzcxzkzVtQ")
    crypto = MSCrypto()
    plaintext = crypto.decrypt(ciphertext)
    assert plaintext == "My password ends in spaces!    "
