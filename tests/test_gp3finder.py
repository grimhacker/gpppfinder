import pytest

from gp3finder.gp3finder import GPPPFinder
from .conftest import not_raises


def test_gp3finder_init_encrypt_only():
    with not_raises(ValueError):
        g = GPPPFinder(auto=False,
                       decrypt=None,
                       encrypt="ENCRYPT",
                       remote=False,
                       outfile="OUTPUTFILE")
    assert g

    with pytest.raises(ValueError) as e:
        GPPPFinder(auto=True,
                   decrypt=None,
                   encrypt="ENCRYPT",
                   remote=False,
                   outfile="OUTPUTFILE")
    assert "only one of encrypt, decrypt, auto" in str(e)

    with pytest.raises(ValueError) as e:
        GPPPFinder(auto=False,
                   decrypt="DECRYPT",
                   encrypt="ENCRYPT",
                   remote=False,
                   outfile="OUTPUTFILE")
    assert "only one of encrypt, decrypt, auto" in str(e)

    with pytest.raises(ValueError) as e:
        GPPPFinder(auto=True,
                   decrypt="DECRYPT",
                   encrypt="ENCRYPT",
                   remote=False,
                   outfile="OUTPUTFILE")
    assert "only one of encrypt, decrypt, auto" in str(e)


def test_gp3finder_init_decrypt_only():
    with not_raises(ValueError):
        g = GPPPFinder(auto=False,
                       decrypt="DECRYPT",
                       encrypt=None,
                       remote=False,
                       outfile="OUTPUTFILE")
    assert g

    with pytest.raises(ValueError) as e:
        GPPPFinder(auto=False,
                   decrypt="DECRYPT",
                   encrypt="ENCRYPT",
                   remote=False,
                   outfile="OUTPUTFILE")
    assert "only one of encrypt, decrypt, auto" in str(e)

    with pytest.raises(ValueError) as e:
        GPPPFinder(auto=True,
                   decrypt="DECRYPT",
                   encrypt=None,
                   remote=False,
                   outfile="OUTPUTFILE")
    assert "only one of encrypt, decrypt, auto" in str(e)

    with pytest.raises(ValueError) as e:
        GPPPFinder(auto=True,
                   decrypt="DECRYPT",
                   encrypt="ENCRYPT",
                   remote=False,
                   outfile="OUTPUTFILE")
    assert "only one of encrypt, decrypt, auto" in str(e)


def test_gp3finder_init_auto_only():
    with not_raises(ValueError):
        g = GPPPFinder(auto=True,
                       decrypt=None,
                       encrypt=None,
                       hosts=["HOST"],
                       share="SHARE",
                       user="USERNAME@DOMAIN",
                       password="PASSWORD",
                       remote=True,
                       outfile="OUTPUTFILE")
    assert g

    with pytest.raises(ValueError) as e:
        GPPPFinder(auto=True,
                   decrypt=None,
                   encrypt="ENCRYPT",
                   hosts=["HOST"],
                   share="SHARE",
                   user="USERNAME@DOMAIN",
                   password="PASSWORD",
                   remote=True,
                   outfile="OUTPUTFILE")
    assert "only one of encrypt, decrypt, auto" in str(e)

    with pytest.raises(ValueError) as e:
        GPPPFinder(auto=True,
                   decrypt="DECRYPT",
                   encrypt=None,
                   hosts=["HOST"],
                   share="SHARE",
                   user="USERNAME@DOMAIN",
                   password="PASSWORD",
                   remote=True,
                   outfile="OUTPUTFILE")
    assert "only one of encrypt, decrypt, auto" in str(e)

    with pytest.raises(ValueError) as e:
        GPPPFinder(auto=True,
                   decrypt="DECRYPT",
                   encrypt="ENCRYPT",
                   hosts=["HOST"],
                   share="SHARE",
                   user="USERNAME@DOMAIN",
                   password="PASSWORD",
                   remote=True,
                   outfile="OUTPUTFILE")
    assert "only one of encrypt, decrypt, auto" in str(e)


def test_gp3finder_init_auto_local_only():
    with not_raises(ValueError):
        g = GPPPFinder(auto=True,
                       decrypt=None,
                       encrypt=None,
                       local=True,
                       remote=False,
                       outfile="OUTPUTFILE")
    assert g

    with pytest.raises(ValueError) as e:
        GPPPFinder(auto=True,
                   decrypt=None,
                   encrypt=None,
                   hosts=["HOST"],
                   share="SHARE",
                   user="USERNAME@DOMAIN",
                   password="PASSWORD",
                   remote=True,
                   local=True,
                   outfile="OUTPUTFILE")
    assert "either local or remote for auto running" in str(e)

    with pytest.raises(ValueError) as e:
        GPPPFinder(auto=True,
                   decrypt=None,
                   encrypt=None,
                   remote=False,
                   local=False,
                   outfile="OUTPUTFILE")
    assert "either local or remote for auto running" in str(e)


def test_gp3finder_init_auto_remote_only():
    with not_raises(ValueError):
        g = GPPPFinder(auto=True,
                       decrypt=None,
                       encrypt=None,
                       hosts=["HOST"],
                       share="SHARE",
                       user="USERNAME@DOMAIN",
                       password="PASSWORD",
                       remote=True,
                       outfile="OUTPUTFILE")
    assert g

    with pytest.raises(ValueError) as e:
        GPPPFinder(auto=True,
                   decrypt=None,
                   encrypt=None,
                   hosts=["HOST"],
                   share="SHARE",
                   user="USERNAME@DOMAIN",
                   password="PASSWORD",
                   remote=True,
                   local=True,
                   outfile="OUTPUTFILE")
    assert "either local or remote for auto running" in str(e)

    with pytest.raises(ValueError) as e:
        GPPPFinder(auto=True,
                   decrypt=None,
                   encrypt=None,
                   remote=False,
                   local=False,
                   outfile="OUTPUTFILE")
    assert "either local or remote for auto running" in str(e)


def test_gp3finder_init_auto_local_requires_local_root():
    with pytest.raises(ValueError) as e:
        GPPPFinder(auto=True,
                   decrypt=None,
                   encrypt=None,
                   local=True,
                   remote=False,
                   local_root=None,
                   outfile="OUTPUTFILE")
    assert "specify local_root" in str(e)


def test_gp3finder_init_auto_remote_requires_hosts():
    with pytest.raises(TypeError) as e:
        GPPPFinder(auto=True,
                   decrypt=None,
                   encrypt=None,
                   share="SHARE",
                   user="USERNAME@DOMAIN",
                   password="PASSWORD",
                   remote=True,
                   outfile="OUTPUTFILE")
    assert "hosts must be a list of strings" in str(e)
    # assert "Must specify host, share and credentials" in str(e)


def test_gp3finder_init_auto_remote_requires_share():
    with pytest.raises(TypeError) as e:
        GPPPFinder(auto=True,
                   decrypt=None,
                   encrypt=None,
                   hosts=["HOST"],
                   user="USERNAME@DOMAIN",
                   password="PASSWORD",
                   remote=True,
                   outfile="OUTPUTFILE")
    assert "share must be a string" in str(e)


def test_gp3finder_init_auto_remote_requires_user():
    with pytest.raises(TypeError) as e:
        GPPPFinder(auto=True,
                   decrypt=None,
                   encrypt=None,
                   hosts=["HOST"],
                   share="SHARE",
                   password="PASSWORD",
                   remote=True,
                   outfile="OUTPUTFILE")
    assert "user must be a string" in str(e)


def test_gp3finder_init_decrypt_accepts_str():
    with not_raises(TypeError):
        g = GPPPFinder(decrypt="SOMESTRING",
                       auto=False,
                       encrypt=None,
                       remote=False,
                       outfile="OUTPUTFILE")
    assert g


def test_gp3finder_init_decrypt_rejects_int():
    with pytest.raises(TypeError) as e:
        GPPPFinder(decrypt=123456,
                   auto=False,
                   encrypt=None,
                   remote=False,
                   outfile="OUTPUTFILE")
    assert "decrypt must be a string" in str(e)


def test_gp3finder_init_encrypt_accepts_str():
    with not_raises(TypeError):
        g = GPPPFinder(decrypt=None,
                       auto=False,
                       encrypt="SOMESTRING",
                       remote=False,
                       outfile="OUTPUTFILE")
    assert g


def test_gp3finder_init_encrypt_rejects_int():
    with pytest.raises(TypeError) as e:
        GPPPFinder(decrypt=None,
                   auto=False,
                   encrypt=123456,
                   remote=False,
                   outfile="OUTPUTFILE")
    assert "encrypt must be a string" in str(e)


def test_gp3finder_init_auto_accepts_bool():
    with not_raises(TypeError):
        g = GPPPFinder(auto=True,
                       decrypt=None,
                       encrypt=None,
                       hosts=["HOST"],
                       user="USERNAME@DOMAIN",
                       password="PASSWORD",
                       share="SHARE",
                       remote=True,
                       outfile="OUTPUTFILE")
        assert g


def test_gp3finder_init_auto_rejects_int():
    with pytest.raises(TypeError) as e:
        GPPPFinder(decrypt=None,
                   auto=123456,
                   encrypt="SOMESTRING",
                   outfile="OUTPUTFILE")
    assert "auto must be a bool" in str(e)


def test_gp3finder_init_remote_accepts_bool():
    with not_raises(TypeError):
        g = GPPPFinder(auto=True,
                       decrypt=None,
                       encrypt=None,
                       hosts=["HOST"],
                       user="USERNAME@DOMAIN",
                       password="PASSWORD",
                       share="SHARE",
                       remote=True,
                       outfile="OUTPUTFILE")
    assert g


def test_gp3finder_init_remote_rejects_int():
    with pytest.raises(TypeError) as e:
        GPPPFinder(auto=True,
                   decrypt=None,
                   encrypt=None,
                   hosts=["HOST"],
                   user="USERNAME@DOMAIN",
                   password="PASSWORD",
                   share="SHARE",
                   remote=123456,
                   outfile="OUTPUTFILE")
    assert "remote must be a bool" in str(e)


def test_gp3finder_init_local_accepts_bool():
    with not_raises(TypeError):
        g = GPPPFinder(auto=True,
                       decrypt=None,
                       encrypt=None,
                       local=True,
                       remote=False,
                       outfile="OUTPUTFILE")
    assert g


def test_gp3finder_init_local_rejects_int():
    with pytest.raises(TypeError) as e:
        GPPPFinder(auto=True,
                   decrypt=None,
                   encrypt=None,
                   local=123456,
                   remote=False,
                   outfile="OUTPUTFILE")
    assert "local must be a bool" in str(e)


def test_gp3finder_init_remote_root_accepts_str():
    with not_raises(TypeError):
        g = GPPPFinder(auto=True,
                       decrypt=None,
                       encrypt=None,
                       hosts=["HOST"],
                       user="USERNAME@DOMAIN",
                       password="PASSWORD",
                       share="SHARE",
                       remote=True,
                       remote_root="REMOTEROOT",
                       outfile="OUTPUTFILE")
    assert g


def test_gp3finder_init_remote_root_rejects_int():
    with pytest.raises(TypeError) as e:
        GPPPFinder(auto=True,
                   decrypt=None,
                   encrypt=None,
                   hosts=["HOST"],
                   user="USERNAME@DOMAIN",
                   password="PASSWORD",
                   share="SHARE",
                   remote=True,
                   remote_root=123456,
                   outfile="OUTPUTFILE")
    assert "remote_root must be a string" in str(e)


def test_gp3finder_init_local_root_accepts_str():
    with not_raises(TypeError):
        g = GPPPFinder(auto=True,
                       decrypt=None,
                       encrypt=None,
                       local=True,
                       remote=False,
                       local_root="LOCALROOT",
                       outfile="OUTPUTFILE")
    assert g


def test_gp3finder_init_local_root_rejects_int():
    with pytest.raises(TypeError) as e:
        GPPPFinder(auto=True,
                   decrypt=None,
                   encrypt=None,
                   local=True,
                   remote=False,
                   local_root=12345,
                   outfile="OUTPUTFILE")
    assert "local_root must be a string" in str(e)


def test_gp3finder_init_outfile_accepts_str():
    with not_raises(TypeError):
        g = GPPPFinder(auto=True,
                       decrypt=None,
                       encrypt=None,
                       local=True,
                       remote=False,
                       local_root="LOCALROOT",
                       outfile="OUTFILE")
    assert g


def test_gp3finder_init_outfile_rejects_int():
    with pytest.raises(TypeError) as e:
        GPPPFinder(auto=True,
                   decrypt=None,
                   encrypt=None,
                   local=True,
                   remote=False,
                   local_root="LOCALROOT",
                   outfile=123456)
    assert "outfile must be a string" in str(e)


def test_gp3finder_init_hosts_accepts_list_of_str():
    with not_raises(TypeError):
        g = GPPPFinder(auto=True,
                       decrypt=None,
                       encrypt=None,
                       hosts=["HOST", "HOST"],
                       user="USERNAME@DOMAIN",
                       password="PASSWORD",
                       share="SHARE",
                       remote=True,
                       outfile="OUTPUTFILE")
    assert g


@pytest.mark.xfail(reason="string is iterable", run=False)
def test_gp3finder_init_hosts_rejects_str():
    with pytest.raises(TypeError) as e:
        GPPPFinder(auto=True,
                   decrypt=None,
                   encrypt=None,
                   hosts="HOST",
                   user="USERNAME@DOMAIN",
                   password="PASSWORD",
                   share="SHARE",
                   remote=True,
                   outfile="OUTPUTFILE")
    assert "hosts must be a list of strings" in str(e)


def test_gp3finder_init_hosts_rejects_list_of_int():
    with pytest.raises(TypeError) as e:
        GPPPFinder(auto=True,
                   decrypt=None,
                   encrypt=None,
                   hosts=[123456, 789],
                   user="USERNAME@DOMAIN",
                   password="PASSWORD",
                   share="SHARE",
                   remote=True,
                   outfile="OUTPUTFILE")
    assert "hosts must be a list of strings" in str(e)


def test_gp3finder_init_share_accepts_str():
    with not_raises(TypeError):
        g = GPPPFinder(auto=True,
                       decrypt=None,
                       encrypt=None,
                       hosts=["HOST"],
                       user="USERNAME@DOMAIN",
                       password="PASSWORD",
                       share="SHARE",
                       remote=True,
                       outfile="OUTPUTFILE")
    assert g


def test_gp3finder_init_share_rejects_int():
    with pytest.raises(TypeError) as e:
        GPPPFinder(auto=True,
                   decrypt=None,
                   encrypt=None,
                   hosts=["HOST"],
                   user="USERNAME@DOMAIN",
                   password="PASSWORD",
                   share=123456,
                   remote=True,
                   outfile="OUTPUTFILE")
    assert "share must be a string" in str(e)


def test_gp3finder_init_user_accepts_str():
    with not_raises(TypeError):
        g = GPPPFinder(auto=True,
                       decrypt=None,
                       encrypt=None,
                       hosts=["HOST"],
                       user="USERNAME@DOMAIN",
                       password="PASSWORD",
                       share="SHARE",
                       remote=True,
                       outfile="OUTPUTFILE")
    assert g


def test_gp3finder_init_user_rejects_int():
    with pytest.raises(TypeError) as e:
        GPPPFinder(auto=True,
                   decrypt=None,
                   encrypt=None,
                   hosts=["HOST"],
                   user=123456,
                   password="PASSWORD",
                   share="SHARE",
                   remote=True,
                   outfile="OUTPUTFILE")
    assert "user must be a string" in str(e)


def test_gp3finder_init_password_accepts_str():
    with not_raises(TypeError):
        g = GPPPFinder(auto=True,
                       decrypt=None,
                       encrypt=None,
                       hosts=["HOST"],
                       user="USERNAME@DOMAIN",
                       password="PASSWORD",
                       share="SHARE",
                       remote=True,
                       outfile="OUTPUTFILE")
    assert g


def test_gp3finder_init_password_rejects_int():
    with pytest.raises(TypeError) as e:
        GPPPFinder(auto=True,
                   decrypt=None,
                   encrypt=None,
                   hosts=["HOST"],
                   user="USERNAME@DOMAIN",
                   password=123456,
                   share="SHARE",
                   remote=True,
                   outfile="OUTPUTFILE")
    assert "password must be a string" in str(e)


def test_gp3finder_init_local_root_default():
    expected = 'C:\\ProgramData\\Microsoft\\Group Policy\\History'
    g = GPPPFinder(auto=True,
                   local=True,
                   decrypt=None,
                   encrypt=None,
                   remote=False,
                   outfile="OUTPUTFILE")
    assert g.local_root == expected


def test_gp3finder_get_creds_backslash():
    username = "domain\\username"
    password = "password"
    expected_domain = "domain"
    expected_user = "username"

    g = GPPPFinder(auto=True,
                   user=username,
                   password=password,
                   hosts=["HOST"],
                   share="SHARE",
                   decrypt=None,
                   encrypt=None,
                   remote=True,
                   outfile="OUTPUTFILE")
    assert g.user == expected_user
    assert g.domain == expected_domain


def test_gp3finder_get_creds_forwardslash():
    username = "domain/username"
    password = "password"
    expected_domain = "domain"
    expected_user = "username"

    g = GPPPFinder(auto=True,
                   user=username,
                   password=password,
                   hosts=["HOST"],
                   share="SHARE",
                   decrypt=None,
                   encrypt=None,
                   remote=True,
                   outfile="OUTPUTFILE")
    assert g.user == expected_user
    assert g.domain == expected_domain


def test_gp3finder_get_creds_at():
    username = "username@domain"
    password = "password"
    expected_domain = "domain"
    expected_user = "username"

    g = GPPPFinder(auto=True,
                   user=username,
                   password=password,
                   hosts=["HOST"],
                   share="SHARE",
                   decrypt=None,
                   encrypt=None,
                   remote=True,
                   outfile="OUTPUTFILE")
    assert g.user == expected_user
    assert g.domain == expected_domain


def test_gp3finder_encrypt():
    plaintext = "3rKb45rV"
    expected = "LehuaWEr7GE2pOY47SZTTK5EA51GcCyOT9ct0h1d7hQ"
    g = GPPPFinder(encrypt=plaintext,
                   auto=False,
                   decrypt=None,
                   remote=False,
                   outfile="OUTPUTFILE")
    _, ciphertext = g._encrypt()
    assert ciphertext == expected


def test_gp3finder_decrypt():
    expected = "3rKb45rV"
    ciphertext = "LehuaWEr7GE2pOY47SZTTK5EA51GcCyOT9ct0h1d7hQ"
    g = GPPPFinder(decrypt=ciphertext,
                   auto=False,
                   encrypt=None,
                   remote=False,
                   outfile="OUTPUTFILE")
    plaintext = g._decrypt()
    assert plaintext == expected


@pytest.mark.requiresmount
def test_gp3finder_autopwn_local(tmp_path, filesystem):
    dir_ = tmp_path / "output"
    dir_.mkdir(exist_ok=True)
    file_path = dir_ / "gp3finder_local.out"
    outfile = str(file_path)
    g = GPPPFinder(auto=True,
                   decrypt=None,
                   encrypt=None,
                   local=True,
                   remote=False,
                   local_root=filesystem,
                   outfile=outfile)
    g.run()
    with open(outfile, "r") as f:
        assert "3rKb45rV" in f.read()


@pytest.mark.requiresmount
def test_gp3finder_autopwn_remote(tmp_path, smbshare):
    creds = smbshare
    dir_ = tmp_path / "output"
    dir_.mkdir(exist_ok=True)
    file_path = dir_ / "gp3finder_remote.out"
    outfile = str(file_path)
    hosts = [creds["host"]]
    user = "{}@{}".format(creds["user"], creds["domain"])
    password = creds["passwd"]
    share = creds["share"]
    g = GPPPFinder(auto=True,
                   decrypt=None,
                   encrypt=None,
                   local=False,
                   remote=True,
                   hosts=hosts,
                   user=user,
                   password=password,
                   share=share,
                   outfile=outfile)
    g.run()
    with open(outfile, "r") as f:
        assert "3rKb45rV" in f.read()
