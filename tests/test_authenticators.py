import pytest
import sys

from .conftest import not_raises

from gp3finder.lib.authenticators import SMBAuthenticator


@pytest.fixture
def creds():
    return {
        "user": "USER_STRING",
        "passwd": "PASSWORD_STRING",
        "domain": "DOMAIN_STRING",
        "share": "SHARE_STRING",
        "host": "HOST_STRING"
    }


@pytest.fixture
def authenticated(smbshare):
    authenticator = SMBAuthenticator(**smbshare)
    if not authenticator.authenticate():
        raise EnvironmentError("Failed to authenticate with valid creds")
    yield authenticator
    authenticator.deauthenticate()


@pytest.fixture
def mapped(authenticated):
    authenticated.map()
    yield authenticated
    authenticated.deauthenticate()


def test_authenticators_authenticate(smbshare):
    authenticator = SMBAuthenticator(**smbshare)
    ret = authenticator.authenticate()
    assert ret is True


def test_authenticators_deauthenticate(authenticated):
    with not_raises(Exception):
        ret = authenticated.deauthenticate()
    assert ret


def test_authenticators_devices(creds):
    authenticator = SMBAuthenticator(**creds)
    devices = authenticator.devices
    # This is not a very robust test...
    if sys.platform == "win32":
        assert "C:" in devices
    else:
        # Expecting an empty list as no files in the temp directory we create.
        assert isinstance(devices, list)


@pytest.mark.requiresmount
def test_authenticators_map(authenticated):
    with not_raises(Exception):
        # On *nix:
        # any problem making mount point directory or mounting results raises
        # an Exception, therefore no exception should mean everything is ok.
        # On Windows:
        # Any Exception raised would be a problem.
        authenticated.map()
    # Successful mount/map should result in a local_device string being set.
    assert authenticated.local_device is not None


@pytest.mark.requiresmount
def test_authenticators_unmap(mapped):
    with not_raises(Exception):
        ret = mapped.deauthenticate()
    assert ret


def test_authenticators_allow_deauth_accepts_bool(creds):
    authenticator = SMBAuthenticator(**creds)
    authenticator._set_allow_deauth(False)
    assert authenticator.allow_deauth is False


def test_authenticators_allow_deauth_rejects_str(creds):
    authenticator = SMBAuthenticator(**creds)
    with pytest.raises(TypeError):
        authenticator._set_allow_deauth("NOT A BOOL")


def test_authenticators_allow_deauth_rejects_int(creds):
    authenticator = SMBAuthenticator(**creds)
    with pytest.raises(TypeError):
        authenticator._set_allow_deauth(1234)


def test_authenticators_requires_host(creds):
    del creds["host"]
    with pytest.raises(ValueError) as e:
        SMBAuthenticator(**creds)
    assert "host required" in str(e)


def test_authenticators_requires_user(creds):
    del creds["user"]
    with pytest.raises(ValueError) as e:
        SMBAuthenticator(**creds)
    assert "user required" in str(e)


def test_authenticators_requires_passwd(creds):
    del creds["passwd"]
    with pytest.raises(ValueError) as e:
        SMBAuthenticator(**creds)
    assert "passwd required" in str(e)


def test_authenticators_requires_domain(creds):
    del creds["domain"]
    with pytest.raises(ValueError) as e:
        SMBAuthenticator(**creds)
    assert "domain required" in str(e)


def test_authenticators_user_accepts_str(creds):
    authenticator = SMBAuthenticator(**creds)
    assert authenticator.user == creds["user"]


def test_authenticators_passwd_accepts_str(creds):
    authenticator = SMBAuthenticator(**creds)
    assert authenticator.passwd == creds["passwd"]


def test_authenticators_domain_accepts_str(creds):
    authenticator = SMBAuthenticator(**creds)
    assert authenticator.domain == creds["domain"]


def test_authenticators_share_accepts_str(creds):
    authenticator = SMBAuthenticator(**creds)
    assert authenticator.share == creds["share"]


def test_authenticators_host_accepts_str(creds):
    authenticator = SMBAuthenticator(**creds)
    assert authenticator.host == creds["host"]


def test_authenticators_host_strips_slashes(creds):
    host = creds["host"]
    creds["host"] = "\\\\{}".format(host)
    authenticator = SMBAuthenticator(**creds)
    assert authenticator.host == host


def test_authenticators_user_rejects_int(creds):
    creds["user"] = 1234
    with pytest.raises(TypeError) as e:
        SMBAuthenticator(**creds)
    assert "user must be a string" in str(e)


def test_authenticators_passwd_rejects_int(creds):
    creds["passwd"] = 1234
    with pytest.raises(TypeError) as e:
        SMBAuthenticator(**creds)
    assert "passwd must be a string" in str(e)


def test_authenticators_domain_rejects_int(creds):
    creds["domain"] = 1234
    with pytest.raises(TypeError) as e:
        SMBAuthenticator(**creds)
    assert "domain must be a string" in str(e)


def test_authenticators_share_rejects_int(creds):
    creds["share"] = 1234
    with pytest.raises(TypeError) as e:
        SMBAuthenticator(**creds)
    assert "share must be a string" in str(e)


def test_authenticators_host_rejects_int(creds):
    creds["host"] = 1234
    with pytest.raises(TypeError) as e:
        SMBAuthenticator(**creds)
    assert "host must be a string" in str(e)
