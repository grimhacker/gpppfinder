import pytest

from gp3finder.lib.data import CPassword


def test_cpassword_decrypt():
    ciphertext = "LehuaWEr7GE2pOY47SZTTK5EA51GcCyOT9ct0h1d7hQ"
    cpassword = CPassword()
    decrypted = cpassword._decrypt_cpassword(ciphertext)
    assert decrypted == "3rKb45rV"


def test_cpassword_cpassword_accepts_string():
    ciphertext = "LehuaWEr7GE2pOY47SZTTK5EA51GcCyOT9ct0h1d7hQ"
    cpassword = CPassword()
    cpassword._set_cpassword(ciphertext)
    assert cpassword.cpassword == ciphertext


def test_cpassword_cpassword_rejects_non_string():
    ciphertext = 1234
    cpassword = CPassword()
    with pytest.raises(Exception) as e:
        cpassword._set_cpassword(ciphertext)
    assert "password must be a string" in str(e.value)


def test_username_accepts_string():
    username = "USERNAME_STRING"
    cpassword = CPassword()
    cpassword._set_user_name(username)
    assert cpassword.user == username


def test_username_converts_non_string():
    username = 1234
    cpassword = CPassword()
    cpassword._set_user_name(username)
    assert str(username) == cpassword.user


def test_cpassword_decrypt_on_init():
    ciphertext = "LehuaWEr7GE2pOY47SZTTK5EA51GcCyOT9ct0h1d7hQ"
    cpassword = CPassword(cpassword=ciphertext)
    assert cpassword.password == "3rKb45rV"


def test_cpassword_init_sets_user():
    username = "USERNAME_STRING"
    cpassword = CPassword(user_name=username)
    assert cpassword.user == username


def test_cpassword_init_sets_cpassword():
    ciphertext = "LehuaWEr7GE2pOY47SZTTK5EA51GcCyOT9ct0h1d7hQ"
    cpassword = CPassword(cpassword=ciphertext)
    assert cpassword.cpassword == ciphertext


def test_cpassword_init_sets_file():
    filename = "FILENAME_STRING"
    cpassword = CPassword(file=filename)
    assert cpassword.file == filename


@pytest.fixture
def cpassword():
    return CPassword(user_name="USERNAME_STRING",
                     cpassword="LehuaWEr7GE2pOY47SZTTK5EA51GcCyOT9ct0h1d7hQ",
                     file="FILENAME_STRING")


def test_cpassword_user_readonly(cpassword):
    with pytest.raises(AttributeError):
        cpassword.user = "EDIT"


def test_cpassword_cpassword_readonly(cpassword):
    with pytest.raises(AttributeError):
        cpassword.cpassword = "EDIT"


def test_cpassword_file_readonly(cpassword):
    with pytest.raises(AttributeError):
        cpassword.file = "EDIT"
