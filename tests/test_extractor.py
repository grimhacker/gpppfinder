import pytest

from gp3finder.lib.extractor import Extractor


def test_extractor_detects_password_in_file(password_file):
    extractor = Extractor(local_file=password_file,
                          remote_file="REMOTE_FILE_STRING")
    assert extractor.password_in_file is True


def test_extractor_ignores_non_password_file(non_password_file):
    extractor = Extractor(local_file=non_password_file,
                          remote_file="REMOTE_FILE_STRING")
    assert extractor.password_in_file is False


def test_extractor_rejects_storing_non_cpassword_instance():
    extractor = Extractor(local_file="LOCAL_FILE_STRING",
                          remote_file="REMOTE_FILE_STRING")
    with pytest.raises(Exception) as e:
        extractor.cpassword = "not a CPassword instance"
    assert len(extractor.cpasswords) == 0
    assert "must be an instance of CPassword" in str(e.value)


def test_extractor_accepts_storing_cpassword_instance(cpassword):
    extractor = Extractor(local_file="LOCAL_FILE_STRING",
                          remote_file="REMOTE_FILE_STRING")
    extractor.cpassword = cpassword
    assert cpassword in extractor.cpasswords


def test_properties_tag_parsing(gppfile):
    # Check:
    #  an iterable is returned
    #  it contains Elements with tag=="Properties"
    #  there are the expected number (one cpassword per properties tag).
    filename, expected_cpassword = gppfile
    extractor = Extractor(local_file=filename,
                          remote_file="REMOTE_FILE_STRING")
    for num, element in enumerate(extractor._get_properties_tags()):
        assert element.tag == "Properties"
    assert num + 1 == expected_cpassword


def test_extractor_extract_password_creates_cpassword(gppfile):
    filename, expected_cpassword = gppfile
    extractor = Extractor(local_file=filename,
                          remote_file="REMOTE_FILE_STRING")
    extractor._extract_password()
    assert len(extractor.cpasswords) == expected_cpassword


def test_extractor_run_detects_password_and_creates_cpassword(gppfile):
    filename, expected_cpassword = gppfile
    extractor = Extractor(local_file=filename,
                          remote_file="REMOTE_FILE_STRING")
    extractor.run()
    assert extractor.password_in_file is True
    assert len(extractor.cpasswords) == expected_cpassword


def test_extractor_extract_from_groups_creates_cpassword(groups_file1):
    filename, expected_cpassword = groups_file1
    extractor = Extractor(local_file=filename,
                          remote_file="REMOTE_FILE_STRING")
    extractor._extract_from_groups()
    assert len(extractor.cpasswords) == expected_cpassword


def test_extractor_extract_from_services_creates_cpassword(services_file1):
    filename, expected_cpassword = services_file1
    extractor = Extractor(local_file=filename,
                          remote_file="REMOTE_FILE_STRING")
    extractor._extract_from_services()
    assert len(extractor.cpasswords) == expected_cpassword


def test_extractor_extract_from_scheduledtasks_creates_cpassword(
        scheduledtasks_file1):
    filename, expected_cpassword = scheduledtasks_file1
    extractor = Extractor(local_file=filename,
                          remote_file="REMOTE_FILE_STRING")
    extractor._extract_from_scheduledtasks()
    assert len(extractor.cpasswords) == expected_cpassword


def test_extractor_extract_from_datasources_creates_cpassword(
        datasources_file1):
    filename, expected_cpassword = datasources_file1
    extractor = Extractor(local_file=filename,
                          remote_file="REMOTE_FILE_STRING")
    extractor._extract_from_datasources()
    assert len(extractor.cpasswords) == expected_cpassword


def test_extractor_extract_from_printers_creates_cpassword(printers_file1):
    filename, expected_cpassword = printers_file1
    extractor = Extractor(local_file=filename,
                          remote_file="REMOTE_FILE_STRING")
    extractor._extract_from_printers()
    assert len(extractor.cpasswords) == expected_cpassword


def test_extractor_extract_from_drives_creates_cpassword(drives_file1):
    filename, expected_cpassword = drives_file1
    extractor = Extractor(local_file=filename,
                          remote_file="REMOTE_FILE_STRING")
    extractor._extract_from_drives()
    assert len(extractor.cpasswords) == expected_cpassword
