FROM python:3

# Install smbclient and sudo.
RUN apt-get update && apt-get install -y smbclient sudo && rm -rf /var/lib/apt/lists/*

# Keeps Python from generating .pyc files in the container
ENV PYTHONDONTWRITEBYTECODE 1

# Turns off buffering for easier container logging
ENV PYTHONUNBUFFERED 1

# Install pip requirements
COPY requirements.txt .
RUN python -m pip install --no-cache-dir -r requirements.txt && rm requirements.txt

# Change the working directory in the container
WORKDIR /app
# Copy the contents of the local directory to the container.
COPY . /app

RUN export PATH=/usr/local/bin/python:${PATH} && python setup.py install

# Switching to a non-root user, please refer to https://aka.ms/vscode-docker-python-user-rights
RUN useradd appuser -m -d /home/appuser && chown -R appuser /app

# Install our package globally in the container. # TODO: this isn't working properly.
# RUN python -m pip install .

# If a different command isn't supplied, supply the help output.
# CMD python gp3finder.py --help

# During debugging, this entry point will be overridden. For more information, please refer to https://aka.ms/vscode-docker-python-debug
ENTRYPOINT ["gp3finder"]
