# GP3Finder - Group Policy Preferences Password Finder.
```
.       .1111...          | Title: Group Policy Preference Password Finder
    .10000000000011.   .. | Author: Oliver Morton
  .00             000...  | Email: grimhacker@grimhacker.com
1                  01..   | Description:
                    ..    | Group Policy Preferences Password Finder.
                   ..     |
GrimHacker        ..      |
                 ..       |
grimhacker.com  ..        |
@grimhacker    ..         |
------------------------------------------------------------------------------
    Copyright (C) 2020  Oliver Morton

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
```

Microsoft published the AES encryption key used to protect ```cpassword```
attributes in Group Policy preference items (CVE-2014-1812 / MS14-025).

This is a cross platform tool to automate the process of finding, extracting
and decrypting passwords stored in Group Policy preference (ms-gppref) items.

It supports connecting to a remote network share (e.g. SYSVOL of a Domain
Controller or C$ of servers/workstations) along with searching the local
filesystem. Instances of ```cpassword``` are reported along with the username
and plaintext password.

Additionally a provided ```cpassword``` can be decrypted and the plaintext
password returned.

Note that by default any Domain User can access the contents of SYSVOL and
therefore retrieve the credentials. Alternatively users can search the local
cache of the preference items stored on their workstation to retrieve
credentials.
Searching remote systems requires administrative privileges (by default).

Further information about this tool and the vulnerability it exploits is
available here: https://grimhacker.com/gp3finder-group-policy-preference-password-finder/


# Installation
## Dependencies
This tools requires:

* the following system requirements:

    - Python3
    - smbclient (if not running on Windows)
    - cifs-utils (if not running on Windows i.e. "mount -t cifs")


* the following Python libraries (installed by setup.py):

    - PyWin32 (if running on Windows)
    - cryptography
    - defusedxml


* the following Python libraries are required for testing/dev:

    - docker
    - pytest


## Install
### Virtualenv
It is recommended to use a virtual environment:
```
python3 -m virtualenv -p python3 venv
```
Activating the virtual environment differs slightly on Windows and Linux.
Windows:
```
venv\Scripts\activate
```
Linux:
```
source venv/bin/activate
```

### Setup.py
Run setup.py to install:
```
python setup.py install
```

You can now execute gp3finder as shown below (ensure you activate your virtual
environment each time you want to run gp3finder).
Once you have finished using gp3finder deactivate your virtual environment
using: ```deactivate```

# Typical Usage
Note: the user this script is run as must have permission to map/mount
shares if running against a remote host.

## Decrypt a given cpassword
```
gp3finder -D CPASSWORD
```

## Automatically find and decrypt GPP files.
The following commands output decrypted cpasswords (from Groups.xml etc)
and list of xml files that contain the word 'password' (for manual review)
to file ('gp3finder.out' by default, this can be changed with -o FILE).

### Find and decrypt cpasswords on domain controller automatically
```
gp3finder -A -t DOMAIN_CONTROLLER -u DOMAIN\USER -p PASSWORD
```
Maps DOMAIN_CONTROLLER's sysvol share with given credentials.

### Find and decrypt cpasswords on the local machine automatically
```
gp3finder -A -l
```
Searches through "C:\ProgramData\Microsoft\Group Policy\History" (by
default) this can be changed with -lr PATH

### Find and decrypt cpasswords on a remote host
```
gp3finder -A -t HOST -u DOMAIN\USER  -p PASSWORD -s C$ -rr "ProgramData\Microsoft\Group Policy\History"
```

### Find and decrypt cpasswords on hosts specified in a file (one per line)
```
gp3finder -A -f HOST_FILE -u DOMAIN\USER  -p PASSWORD -s C$ -rr "ProgramData\Microsoft\Group Policy\History"
```

# Additional Options:
See: ```gp3finder --help```

# Docker
GP3Finder can be run from within a Docker container. This is a primarily intended
for running the testcases, but should work "in the wild" as well.
As the ```mount``` command is used to mount remote Windows shares the
container requires additional privilges - specifically the SYS_ADMIN capability.
Additionally the container must be excluded from apparmor (on Ubuntu).

Note that (perhaps confusingly) the image and container names in the commands
below are ```gp3finder```.

## Running the test cases
Some of the authentication test cases require a SAMBA share, this is accomplished
using a docker container that is created by the test case fixture. Therefore the
gp3finder container needs access to the docker socket via a volume mount.
Note that SAMBA ports must be available to bind to on the docker host.
The default entrypoint for the container must be overridden to execute pytest:
```
docker build --rm --pull -f "Dockerfile" -t "gp3finder:latest" "." &&
docker run --rm -v /var/run/docker.sock:/var/run/docker.sock -it --cap-add=SYS_ADMIN --security-opt apparmor:unconfined --entrypoint python gp3finder -m pytest -v
```

## Running GP3Finder via Docker
The options above can be used within the docker container. For example:
```
docker run --rm -it --cap-add=SYS_ADMIN --security-opt apparmor:unconfined gp3finder \
-A -t DOMAIN_CONTROLLER -u DOMAIN\USER -p PASSWORD
```

The "local machine" options can be used used if the appropriate volume mount is used.
```
docker run --rm -it -v /some/local/path:/mnt --cap-add=SYS_ADMIN --security-opt apparmor:unconfined gp3finder \
-A -l -lr /mnt
```

# Windows EXE
pyinstaller can be used create a standalone executable:
```
python -m pip install pyinstaller
pyinstaller.exe --clean --onefile --icon=gp3finder\favicon.ico -n gp3finder .\cli.py
```
The exe is created in the ```dist``` directory.

## Download
A precompiled executable is available in the Downloads section on bitbucket:
https://bitbucket.org/grimhacker/gpppfinder/downloads/
