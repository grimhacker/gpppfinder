'''
.       .1111...          | Title: reporter.py
    .10000000000011.   .. | Author: Oliver Morton
 .00              000...  | Email: grimhacker@grimhacker.com
1                  01..   | Description:
                    ..    | reporter for gpppfinder.py
                   ..     |
GrimHacker        ..      |
                 ..       |
grimhacker.com  ..        |
@grimhacker    ..         |
----------------------------------------------------------------------------
GPPPFinder - Group Policy Preference Password Finder
    Copyright (C) 2020  Oliver Morton

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
'''

import logging

from gp3finder.lib.data import CPassword


class Reporter(object):
    """Reporter to output cpasswords and files with 'password' in them to file.
    """
    def __init__(self, **kwargs):
        """Initialise reporter object.

        Keyword Arguments:
            cpasswords {list} -- list of CPassword instances.
            password_files {list} -- list of strings of filenames containing
                                     "password".
            output_file {str}  -- filename to output data to.

        Raises:
            TypeError: raised when cpasswords is passed anything other than
                       list of CPassword instances.
            TypeError: raised when password_files is passed anything other
                       than list of strings.
            TypeError: raised when output_file is passed anything other than a
                       string.
        """
        self.log = logging.getLogger(__name__)
        if all(
                isinstance(cpassword, CPassword)
                for cpassword in kwargs.get('cpasswords')):
            self._cpasswords = kwargs['cpasswords']
        else:
            raise TypeError("cpasswords must be a list of CPassword instances")

        if all(
                isinstance(file_, str)
                for file_ in kwargs.get('password_files')):
            self._password_files = kwargs['password_files']
        else:
            raise TypeError(
                "password_files must be a list of strings (file names).")

        if isinstance(kwargs.get('output_file'), str):
            self._output_file = kwargs['output_file']
        else:
            raise TypeError("output_file must be a string")

    @property
    def cpasswords(self):
        """Read only property, returns list of CPassword instances.

        Returns:
            list -- list of CPassword instances.
        """
        return self._cpasswords

    @property
    def password_files(self):
        """Read only property, returns list of strings containing filenames.

        Returns:
            list -- list of strings containing filenames.
        """
        return self._password_files

    @property
    def output_file(self):
        """Read only property, returns output filename.

        Returns:
            str -- output filename.
        """
        return self._output_file

    def run(self):
        """Run Reporter instance, output cpassword and password_files data to out_file.
        Output file is colon separated user:password:cpassword:file.
        CPassword lines are fully populated,
        other lines contain filenames of files that contain "password" .
        """
        # TODO: decide on a decent output format.
        # This is bad... grepable... but bad...
        self.log.debug("creating output file '{0}'...".format(
            self.output_file))
        with open(self.output_file, "w") as f:
            f.writelines("user:password:cpassword:file\n")
            for cpassword in self.cpasswords:
                # write username, password, cpassword, file name
                # to output file.
                f.writelines("{0}:{1}:{2}:{3}\n".format(
                    cpassword.user, cpassword.password, cpassword.cpassword,
                    cpassword.file))
            for file_ in self.password_files:
                # write file name of each file that contains 'password' to
                # output file
                f.writelines(":::{0}\n".format(file_))
        self.log.info("{0} cpasswords, {1} files containing 'password'".format(
            len(self.cpasswords), len(self.password_files)))
