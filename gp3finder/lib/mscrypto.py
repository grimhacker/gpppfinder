'''
.       .1111...          | Title: mscrypto.py
    .10000000000011.   .. | Author: Oliver Morton
 .00              000...  | Email: grimhacker@grimhacker.com
1                  01..   | Description:
                    ..    | Implements encryption and decryption using
                   ..     | the AES key published by Microsoft.
GrimHacker        ..      |
                 ..       |
grimhacker.com  ..        |
@grimhacker    ..         |
----------------------------------------------------------------------------
GPPPFinder - Group Policy Preference Password Finder
    Copyright (C) 2020  Oliver Morton

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
'''
import logging

from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat.backends import default_backend
from base64 import b64decode
from base64 import b64encode


class MSCrypto(object):
    """Decrypt cpassword to plaintext password using the AES encryption key
    released by Microsoft (http://msdn.microsoft.com/en-us/library/2c15cbf0-f086-4c74-8b70-1f2fa45dd4be%28v=PROT.13%29#endNote2)

    Originally adapted from:
     - http://www.leonteale.co.uk/decrypting-windows-2008-gpp-user-passwords-using-gpprefdecrypt-py/ (http://pastebin.com/TE3fvhEh)
     - http://carnal0wnage.attackresearch.com/2012/10/group-policy-preferences-and-getting.html
    """
    def __init__(self):
        """Initialise with the known IV and Key.
        """
        self.log = logging.getLogger(__name__)

        self._iv = b'\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0'
        # 16 bytes of literal zero bytes  - this is the old default from
        # pycrypto which was removed and broke gpprefdecrypt.py
        # (see https://bugs.launchpad.net/pycrypto/+bug/1018283 and
        # https://github.com/dlitz/pycrypto/commit/411f60f58cea79f7e93476ba0c069b80a2a4c1a0)

        # From MSDN: http://msdn.microsoft.com/en-us/library/2c15cbf0-f086-4c74-8b70-1f2fa45dd4be%28v=PROT.13%29#endNote2
        key = ""  # this is here purely so that the next two lines align
        key = key + "4e 99 06 e8  fc b6 6c c9  fa f4 93 10  62 0f fe e8"
        key = key + "f4 96 e8 06  cc 05 79 90  20 9b 09 a4  33 b6 6c 1b"
        key = key.replace(" ", "")
        self._key = bytes.fromhex(key)

    @property
    def iv(self):
        """Read only property to return the IV required for cryptographic functions.

        Returns:
            bytes -- Initialisation vector (unencoded).
        """
        return self._iv

    @property
    def key(self):
        """Read only property to return the Key required for cryptographic functions.

        Returns:
            bytes -- Cryptographic key (unencoded).
        """
        return self._key

    def _encrypt_plaintext(self, plaintext):
        """Encrypts the supplied plaintext using AES after required
        re-encoding and padding.

        First the plaintext is encoded as UTF-16LE (i.e. Microsoft's unicode)
        so that we have the plaintext as bytes.
        Next the plaintext bytes are padded to the CBC blocksize.
        (Note the padding character is the character whose ordinal is equal to
        the amount of padding required.)
        Finally the cryptography library is used to perform encryption of the
        padded plaintext.

        Arguments:
            plaintext {str} -- String to encrypt

        Returns:
            bytes -- Ciphertext - Unencoded encrypted result, note may contain
                                non-printable characters.
        """
        self.log.debug("plaintext: " + plaintext)
        utf16encoded_plaintext = plaintext.encode("utf-16le")
        # needs le because Microsoft apparently (http://stackoverflow.com/questions/20212487/utf-8-convert-to-utf-16)
        # otherwise a BOM is prepended (fffe)
        self.log.debug("utf-16le_encoded_plaintext (hex): " +
                       utf16encoded_plaintext.hex())

        # pad plaintext to blocksize
        block_size = 16  # block size for cbc
        padding_length = block_size - len(
            utf16encoded_plaintext
        ) % block_size  # calculate length of padding required.
        padding_char = chr(padding_length).encode()  # get character to pad with
        self.log.debug("padding char is (hex): " + padding_char.hex())
        padding = padding_char * padding_length  # getting padding
        padded_plaintext = utf16encoded_plaintext + padding  # pad plaintext
        self.log.debug("padded_plaintext (hex): " +
                       padded_plaintext.hex())

        backend = default_backend()
        cipher = Cipher(algorithms.AES(self.key),
                        modes.CBC(self.iv),
                        backend=backend)
        encryptor = cipher.encryptor()
        ct = encryptor.update(padded_plaintext) + encryptor.finalize()
        return ct

    def _encode_ciphertext(self, ciphertext):
        """Base64 encode given text and remove padding.

        Arguments:
            ciphertext {str} -- unencoded ciphertext

        Returns:
            str -- base64 encoded ciphertext with padding removed.
        """
        # base64 encode ciphertext and strip "=" padding
        encoded_ciphertext = b64encode(ciphertext).strip(b"=")
        # decode encoded_ciphertext bytes as "utf-8" and return
        return encoded_ciphertext.decode("utf-8")

    def encrypt(self, plaintext):
        """Encrypt and encode given plaintext to "cpassword".

        WARNING: THIS IS NOT SECURE ENCRYPTION BECAUSE THE KEY IS PUBLICALLY KNOWN.
        IT MUST NOT BE USED FOR ANY APPLICATION REQURING ANY SECURITY!

        Arguments:
            plaintext {str} -- String of plaintext password to
                               be encrypted

        Raises:
            Exception: execeptions are raised if encryption or encoding fail.

        Returns:
            tuple -- (warning, encoded_ciphertext)
                warning -- str containing a warning about the insecurity of
                           this encryption
                encoded_ciphertext -- str result of the operation.
        """
        warning = """
        WARNING: THIS IS NOT SECURE ENCRYPTION BECAUSE THE KEY IS PUBLICALLY KNOWN.
        DO NOT USE FOR ANYTHING! EVER!
        """
        self.log.debug("Padding and encrypting plaintext...")

        try:
            ciphertext = self._encrypt_plaintext(plaintext)
        except Exception as e:
            raise Exception("Failed to encrypt plaintext. {0}".format(e))
        else:
            self.log.debug("encoding and stripping padding...")
            try:
                encoded_ciphertext = self._encode_ciphertext(ciphertext)
            except Exception as e:
                raise Exception("Failed to encode ciphertext. {0}".format(e))
            else:
                return warning, encoded_ciphertext

    def _decode_cpassword(self, encoded_ciphertext):
        """Base64 decode the given text after applying the required amount of
        padding characters.

        Arguments:
            encoded_cpassword {str} -- base64 encoded text with padding
                                       characters removed

        Raises:
            Exception: exceptions are raised if padding or decoding fail.

        Returns:
            bytes -- decoded text
        """
        self.log.debug("padding and decoding cpassword...")
        try:
            # Add padding to the base64 string so that it is the correct
            # length for decoding
            encoded_ciphertext += "=" * (((4 - len(encoded_ciphertext)) % 4) % 4)
        except Exception as e:
            raise Exception("Error padding encoded cpassword. {0}".format(e))
        else:
            try:
                decoded_cpassword = b64decode(encoded_ciphertext)
            except Exception as e:
                raise Exception(
                    "Error base64 decoding cpassword. {0}".format(e))
            else:
                return decoded_cpassword

    def _decrypt(self, ciphertext):
        """Decrypts given ciphertext using AES and removes the padding characters.

        First the cryptography library is used to decrypt the cipher text.
        Next the padding is removed by getting the ordinal of the final
        character and removing that number of bytes from the string.
        Finally the plaintext is decoded as UTF-16LE and returned.

        Arguments:
            ciphertext {bytes} -- unencoded ciphertext (i.e. base64 decoded).

        Raises:
            Exception: raises exceptions if decryption or padding removal
                       fails.

        Returns:
            str -- plaintext
        """
        self.log.debug("decrypting decoded cpassword...")
        try:
            backend = default_backend()
            cipher = Cipher(algorithms.AES(self.key),
                            modes.CBC(self.iv),
                            backend=backend)
            decryptor = cipher.decryptor()
            decryption = decryptor.update(ciphertext) + decryptor.finalize()
            self.log.debug("decryption (hex): " + decryption.hex())
        except Exception as e:
            raise Exception("Error decrypting ciphertext. {0}".format(e))
        else:
            try:
                # The final byte indicates the amount of padding.
                # Remove that amount of padding from the decryption and
                # decode as utf-16le to give a readable string.
                encoded_plaintext = decryption[:-decryption[-1]]
                plaintext = encoded_plaintext.decode("utf-16le")
            except Exception as e:
                raise Exception(
                    "Error converting decryption to readable form. {0}".format(
                        e))
            else:
                self.log.debug("plaintext decoded utf-16le: " + plaintext)
                return plaintext

    def decrypt(self, encoded_cpassword):
        """Decode and decrypt given cpassword.

        Arguments:
            encoded_ciphertext {str} -- cpassword from
                                        Microsoft Group Policy Preferences

        Raises:
            Exception: raises execptions if decoding or decryption fails.

        Returns:
            str -- plaintext
        """
        try:
            decoded_cpassword = self._decode_cpassword(encoded_cpassword)
        except Exception as e:
            raise Exception("Failed to decode cpassword. {0}".format(e))
        else:
            try:
                plaintext = self._decrypt(decoded_cpassword)
            except Exception as e:
                raise Exception("Failed to decrypt ciphertext. {0}".format(e))
            else:
                return plaintext


if __name__ == "__main__":
    import argparse

    logging.basicConfig(level=logging.DEBUG,
                        format="%(levelname)s: %(module)s: %(message)s")

    parser = argparse.ArgumentParser(
        description="MSCrypto encryption/decryption of cpassword.")
    mutex = parser.add_mutually_exclusive_group()
    mutex.add_argument("-e", "--encrypt", help="String to encrypt.")
    mutex.add_argument("-d", "--decrypt", help="Decrypt cpassword.")
    args = parser.parse_args()

    crypto = MSCrypto()
    if args.encrypt:
        warning, encrypted = crypto.encrypt(args.encrypt)
        print(warning)
        print(encrypted)
    elif args.decrypt:
        print(crypto.decrypt(args.decrypt))
    else:
        print("Specify string to encrypt or cpassword to decrypt.")
        parser.print_usage()
