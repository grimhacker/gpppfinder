'''
.       .1111...          | Title: data.py
    .10000000000011.   .. | Author: Oliver Morton
 .00              000...  | Email: grimhacker@grimhacker.com
1                  01..   | Description:
                    ..    | CPassword stores cpassword and username and
                   ..     | uses MSCrypto to decrypt cpassword and stores
GrimHacker        ..      | as plaintext password.
                 ..       |
grimhacker.com  ..        |
@grimhacker    ..         |
----------------------------------------------------------------------------
GPPPFinder - Group Policy Preference Password Finder
    Copyright (C) 2020  Oliver Morton

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
'''
import logging

from gp3finder.lib.mscrypto import MSCrypto


class CPassword(object):
    """Object to store cpassword data. Automatically decrypts cpassword to
    plaintext.
    """
    def __init__(self, **kwargs):
        """Initialise CPassword. Store provided user_name and cpassword,
        automatically decrypt cpassword.

        Keyword Arguments:
            cpassword {str} -- UTF-8 encoded string. This should be the value
                                from the cpassword attribute of various
                                Microsoft Group Policy Preferences.
            user_name {str} -- username to store
            file {str}  -- filename from which cpassword and user_name
                           originated.
        """
        self.log = logging.getLogger(__name__)
        self._password = None
        # important that self._password is before cpassword as
        # self._set_cpassword will override it.
        self._set_cpassword(kwargs.get('cpassword'))
        self._user_name = kwargs.get('user_name', None)
        # if not isinstance(self._user_name, basestring):
        #     #raise Exception("user_name must be a string")
        # # nope can set by uid so no username...
        # # need to capture that in the future
        #     self._user_name = str(self._user_name)
        self._file = kwargs.get('file', None)

    def _set_cpassword(self, cpassword):
        """Validate cpassword is a string and store it. Decrypt cpassword and
        store the plaintext password.

        Arguments:
            cpassword {str} -- UTF-8 encoded string. This should be the value
                               from the cpassword attribute of various
                               Microsoft Group Policy Preferences.

        Raises:
            TypeError: raised if cpassword is not a string.
            ValueError: raised if cpassword failes to decrypt.
        """
        # self.log.debug("setting cpassword. {0}".format(cpassword))
        if not isinstance(cpassword, str) and cpassword is not None:
            raise TypeError("cpassword must be a string or None")
        else:
            # set the cpassword (or an empty string if there is no cpassword)
            self._cpassword = cpassword or ""
            if not cpassword:
                # if there is no cpassword, set the password to an empty string
                self._set_password("")
            else:
                try:
                    # decrypt the cpassword and set the plaintext password
                    # to self.
                    self._set_password(self._decrypt_cpassword(cpassword))
                except Exception as e:
                    raise ValueError(
                        "Error decrypting cpassword and setting to self. {0}".
                        format(e))

    def _set_user_name(self, user_name):
        """Ensure user_name is a string and store it.

        Arguments:
            user_name {str} -- username to store.

        Raises:
            Exception: [description]
        """
        if isinstance(user_name, str):
            self._user_name = user_name
        else:
            self._user_name = str(user_name)
            # raise Exception("user_name must be a string")
            # # nope, can set by uid so no username...
            # # need to capture that in the future....

    def _set_password(self, password):
        """Validate password is a string and store it.

        Arguments:
            password {str} -- plaintext password to store.

        Raises:
            TypeError: raised when password is not a string.
        """
        if isinstance(password, str):
            self._password = password
        else:
            raise TypeError("password must be a string or None")

    @property
    def file(self):
        """Read only property to return filename.

        Returns:
            str -- filename
        """
        return self._file

    @property
    def user(self):
        """Read only property to return username

        Returns:
            str -- username
        """
        return self._user_name

    @property
    def cpassword(self):
        """Read only property to return cpassword.

        Returns:
            str -- cpassword
        """
        return self._cpassword

    @property
    def password(self):
        """Read only property to return plaintext password.

        Returns:
            str -- plaintext password
        """
        return self._password

    def _decrypt_cpassword(self, cpassword):
        """Decrypt cpassword using MSCrypto.

        Arguments:
            cpassword {str} -- UTF-8 encoded string. This should be the value
                               from the cpassword attribute of various
                               Microsoft Group Policy Preferences.

        Returns:
            str -- plaintext password
        """
        self.log.debug("decrypting cpassword...")
        crypto = MSCrypto()  # initialise an instance of Decryptor
        return crypto.decrypt(
            cpassword
        )  # run decryptor with the cpassword to get the plaintext.
