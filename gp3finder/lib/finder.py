'''
.       .1111...          | Title: finder.py
    .10000000000011.   .. | Author: Oliver Morton
 .00              000...  | Email: grimhacker@grimhacker.com
1                  01..   | Description:
                    ..    | Finds files and uses Extractor to get list
                   ..     | of files with 'password' in them and list of
GrimHacker        ..      | CPassword objects.
                 ..       |
grimhacker.com  ..        |
@grimhacker    ..         |
----------------------------------------------------------------------------
GPPPFinder - Group Policy Preference Password Finder
    Copyright (C) 2020  Oliver Morton

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
'''

import logging
import os
import fnmatch

from gp3finder.lib.data import CPassword
from gp3finder.lib.extractor import Extractor
from gp3finder.lib.authenticators import SMBAuthenticator


class Finder(object):
    """Find Group Policy Preference files containing passwords.

    Searches provided path using provided regex for files containing passwords.
    """
    def __init__(self, **kwargs):
        """Initialise Finder with path and credentials.

        Keyword Arguments:
            local_root {str} -- Local root file path to start search.
            remote_root {str} -- Remote root file path to start search.
            host {str} -- Remote SMB server to connect to.
            user {str} -- Username for SMB server authentication.
            password {str} -- Password for SMB server authenticaiton.
            domain {str} -- Domain for SMB server authentication.
            share {str} -- Share for SMB server authentication.

        Raises:
            TypeError: raised when local_root is not str or None.
            TypeError: raised when host is not str or None.
            TypeError: raised when user is not str
            TypeError: raised when password is not str
            TypeError: raised when domain is not str
            TypeError: raised when share is not str
            ValueError: raised when both local_root and host are both provided.
        """
        self.log = logging.getLogger(__name__)
        self._local_root = kwargs.get("local_root", None)
        self._remote_root = kwargs.get("remote_root", "")
        self._cpasswords = []
        self._password_files = []
        self._files = []

        if not (isinstance(self._local_root, str)
                or self._local_root is None):
            raise TypeError("local_root must be a string or None")

        self._host = kwargs.get("host", None)
        if not (isinstance(self._host, str) or self._host is None):
            raise TypeError("host must be a string or None")

        self._user = kwargs.get("user", None)
        if not (isinstance(self._user, str) or self._user is None):
            raise TypeError("user must be a string")

        self._password = kwargs.get("password", None)
        if not (isinstance(self._password, str)
                or self._password is None):
            raise TypeError("password must be a string")

        self._domain = kwargs.get("domain", self._host)
        if not (isinstance(self._domain, str) or self._domain is None):
            raise TypeError("domain must be a string")

        self._share = kwargs.get("share", "sysvol")
        if not (isinstance(self._share, str) or self._share is None):
            raise TypeError("share must be a string")

        if (self._local_root
                and self._host) or not (self._local_root or self._host):
            raise ValueError("Must specify only one of host and local_root.")

    @property
    def user(self):
        """Read only property, returning username for SMB authentication.

        Returns:
            str -- username for SMB authentication.
        """
        return self._user

    @property
    def password(self):
        """Read only property, returning password for SMB authentication.

        Returns:
            str -- password for SMB authentication.
        """
        return self._password

    @property
    def domain(self):
        """Read only property, returning domain for SMB authentication.

        Returns:
            str -- domain for SMB authentication.
        """
        return self._domain

    @property
    def host(self):
        """Read only property, returning host for SMB authentication.

        Returns:
            str -- SMB server
        """
        return self._host

    @property
    def share(self):
        """Read only property, SMB share to connect to and search.

        Returns:
            str -- SMB share to search.
        """
        return self._share

    @property
    def local_root(self):
        """Read only property, path on local file system to use as root of search.

        Returns:
            str -- path on local file system to use as root of search.
        """
        return self._local_root

    @property
    def remote_root(self):
        """Read only property, path on remote file system to use as root of search.

        Returns:
            str -- path on remote file system to use as root of search.
        """
        return self._remote_root

    @property
    def files(self):
        """List of files found by self._find_files.

        Setter accepts string or list of strings.

        Raises:
            ValueError: raised when files have not yet been searched for.
            TypeError: raised by setter when passed anything other than string
                       or list of strings.

        Returns:
            list -- filename strings within a list.
        """
        if self._files is None:
            raise ValueError("Files haven't been searched for.")
        else:
            return self._files

    @files.setter
    def files(self, file_):
        if self._files is None:
            self._files = []
        if isinstance(file_, list) and all(
                isinstance(f, str) for f in file_):
            self._files += file_
        elif isinstance(file_, str):
            self._files.append(file_)
        else:
            raise TypeError("file to set must be list of strings or a string!")

    @property
    def password_files(self):
        """List of files identified containing 'password'.

        Setter accepts string or list of strings.

        Raises:
            TypeError: raised by setter when passed anything other than string
                       or list of strings.

        Returns:
            list -- filename strings within list.
        """
        return self._password_files

    @password_files.setter
    def password_files(self, file_):
        if isinstance(file_, list) and all(
                isinstance(f, str) for f in file_):
            self._password_files += file_
        elif isinstance(file_, str):
            self._password_files.append(file_)
        else:
            raise TypeError("file to set must be list of strings or a string!")

    @property
    def cpasswords(self):
        """List of CPassword instances extracted from filesystem.

        Setter accepts CPassword instance or list of CPassword instances.

        Raises:
            TypeError: raised by setter when passed anything other than
                       CPassword of list of CPassword.

        Returns:
            list -- list of CPassword instances.
        """
        return self._cpasswords

    @cpasswords.setter
    def cpasswords(self, cpasswords):
        """Add cpasswords list to self."""
        if isinstance(cpasswords, list) and all(
                isinstance(cpassword, CPassword) for cpassword in cpasswords):
            self._cpasswords += cpasswords
        elif isinstance(cpasswords, CPassword):
            self._cpasswords.append(cpasswords)
        else:
            raise TypeError(
                "cpasswords to set must be list of CPassword or a CPassword!")

    def _find_files(self, path, pattern="*"):
        """Find files matching pattern recursively,
        starting at the given path.

        Arguments:
            path {str} -- root of filepath to search

        Keyword Arguments:
            pattern {str} -- unix shell-style wildcards (see fnmatch
                             documentation) (default: {"*"})

        Raises:
            OSError: raised if os.walk errors iterating through filesystem.
        """
        self.log.debug("Starting search at: {0} for {1}".format(path, pattern))
        for root, _, files in os.walk(path):
            for name in files:
                if fnmatch.fnmatch(name, pattern):
                    file_ = os.path.join(root, name)
                    self.log.debug("Found file '{0}'".format(file_))
                    self.files = file_

    def _process_files(self, remote_path, local_path):
        """Search each file in self.files for password using Extractor.

        Sets cpasswords from Extractor to self.cpasswords.
        Adds remote filepath to self.password_files if Extractor found
        a password.

        Arguments:
            remote_path {str} -- Remote root file path used for search.
            local_path {str} -- Local root file path used for search
        """
        def create_filenames(filename, local_path, remote_path):
            """Creates the reusable remote file name.

            Replaces the first instance of local_path in filename with
            remote_path.

            Arguments:
                filename {str} -- local version of filename.
                local_path {str} -- root of local filesystem used for search.
                remote_path {str} -- root of remote filesystem used for search.

            Returns:
                tuple -- local filename {str}, remote filename {str}
            """
            remote_filename = filename.replace(local_path, remote_path, 1)
            return filename, remote_filename

        for file_ in self.files:
            # TODO: consider threading this.
            local_file, remote_file = create_filenames(file_, local_path,
                                                       remote_path)
            try:
                extractor = Extractor(local_file=local_file,
                                      remote_file=remote_file)
            except Exception as e:
                self.log.warning(
                    "Error creating instance of Extractor for '{0}'. {1}".
                    format(remote_file, e))
            else:
                try:
                    extractor.run()
                except NotImplementedError as e:
                    self.log.warning(
                        "Could not extract password from '{0}'. '{1}'".format(
                            remote_file, e))
                except Exception as e:
                    self.log.warning(
                        "Error extracting cpasswords from '{0}'. {1}".format(
                            remote_file, e))

                try:
                    self.cpasswords = extractor.cpasswords
                except Exception as e:
                    self.log.warning(
                        "Error adding cpasswords to self from '{0}'. {1}".
                        format(remote_file, e))
                try:
                    if extractor.password_in_file:
                        self.password_files = remote_file
                except Exception as e:
                    self.log.warning(
                        "Error getting password_in_file for '{0}'. {1}".format(
                            remote_file, e))

    def _find(self, local_path, remote_path=None, pattern="*.xml"):
        """Find and process files.

        Arguments:
            local_path {str} -- root of local filesystem to search.

        Keyword Arguments:
            remote_path {str} -- root of remote filesystem (default: {None})
            pattern {str} -- unix shell-style wildcards (see fnmatch)
                             (default: {"*.xml"})

        Raises:
            OSError: raised by self._find_files if any errors searching
                     filesystem.
        """
        if remote_path is None:
            remote_path = local_path

        self.log.info(
            "Finding files matching '{0}' on share...".format(pattern))
        try:
            self._find_files(pattern=pattern, path=local_path)
        except Exception:
            raise
        else:
            self.log.info("Processing files...")
            try:
                self._process_files(local_path=local_path,
                                    remote_path=remote_path)
            except Exception:
                raise

    def run(self):
        """Run GPPPFinder to find, extract and decrypt passwords.

        If host has been specified, an SMB connection is created using
        SMBAuthenticator.
        Local root (or mount point of remote filesystem) is searched for
        Group Policy Preference files and any cpasswords extracted and
        decrypted.

        Files containing passwords are listed on self.password_files.
        CPassword instances are listed on self.cpasswords.

        Raises:
            Exception: raised or reraised when critical errors occur.
        """
        if self.host:
            self.log.info("Mapping share...")
            try:
                mapper = SMBAuthenticator(host=self.host,
                                          user=self.user,
                                          passwd=self.password,
                                          domain=self.domain,
                                          share=self.share)
            except Exception as e:
                raise
            else:
                try:
                    mapper.map()
                except Exception as e:
                    raise Exception("Error running mapper. {0}".format(e))
                else:
                    try:
                        device = mapper.local_device
                        local_path = os.path.join(device, self.remote_root)
                        remote_path = "{sep}{sep}{path}".format(
                            sep=os.path.sep,
                            path=os.path.join(self.host, self.share,
                                              self.remote_root))
                    except Exception:
                        raise
                    else:
                        # TODO: the file names we are interested in are fixed
                        # and registered in ldap, just pull them from there?
                        # (fall back to this if ldap lookup fails.)
                        # Probably still need to check the filesystem in case
                        # it isn't stored in LDAP.
                        self.log.debug("share mapped to '{0}'".format(device))
                        self._find(local_path, remote_path)
                    self.log.info("Deauthenticating...")
                    try:
                        mapper.deauthenticate()
                    except Exception as e:
                        self.log.warning(
                            "Failed to deauthenticate. {0}".format(e))
                    else:
                        self.log.debug("deauthenticated.")
        elif self.local_root:
            self._find(self.local_root)
