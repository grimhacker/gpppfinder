'''
.       .1111...          | Title: extractor.py
    .10000000000011.   .. | Author: Oliver Morton
 .00              000...  | Email: grimhacker@grimhacker.com
1                  01..   | Description:
                    ..    | Checks if files contain 'password'
                   ..     | Extracts cpasswords and usernames from known
GrimHacker        ..      | files.
                 ..       |
grimhacker.com  ..        |
@grimhacker    ..         |
----------------------------------------------------------------------------
GPPPFinder - Group Policy Preference Password Finder
    Copyright (C) 2020  Oliver Morton

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
'''

import logging

from defusedxml import ElementTree as ET

from gp3finder.lib.data import CPassword


class Extractor(object):
    """Checks if "password" is in file. Attempts to parse the given file as
    XML and extract the cpassword and username.

    Creates CPassword instances if cpassword is found.
    """
    def __init__(self, **kwargs):
        """Initialise Extractor.

        Keyword Arguments:
            local_file {str} -- File to parse, file must be accessible locally
                                (i.e. on a mounted filesystem).
            remote_file {str} -- Meaningful filename which can be used to
                                 locate this file once the filesystem has been
                                 unmounted.

        Raises:
            TypeError: local_file must be a string.
        """
        self.log = logging.getLogger(__name__)
        if isinstance(kwargs.get("local_file"), str):
            self._local_file = kwargs.get('local_file')
        else:
            raise TypeError("file must be a string")
        self._remote_file = kwargs.get('remote_file', None)
        self._password_in_file = None
        self._cpasswords = []

    @property
    def password_in_file(self):
        """Check if local_file contains password and return True or False.

        The file is only read once and the result stored for future use.
        The setter for this propery restricts the result to True or False.

        Raises:
            TypeError: property setter raises TypeError if result is not True
                       or False.

        Returns:
            bool -- True means the file appears to contain a password,
                    False means it appears not to.
        """
        if self._password_in_file is None:
            with open(self.local_file, "r") as f:
                if "password" in f.read():
                    self.password_in_file = True
                else:
                    self.password_in_file = False
        return self._password_in_file

    @password_in_file.setter
    def password_in_file(self, result):
        if result in [True, False]:
            self._password_in_file = result
        else:
            raise TypeError("result must be True or False")

    @property
    def remote_file(self):
        """Read only property to access the remote_file attribute of this class.

        Returns:
            str --  remote_file provided when this class was created.
        """
        return self._remote_file

    @property
    def local_file(self):
        """Read only property to access the local_file attribute of this class.

        Returns:
            str --  local_file provided when this class was created.
        """
        return self._local_file

    @property
    def cpasswords(self):
        """Provides access to the list of CPassword instances in self._cpasswords.

         Arguments:
            cpassword {CPassword} -- Setter for this property expects a
                                     initialised CPassword instance.

        Returns:
            list -- list of CPassword instances.
        """
        return self._cpasswords

    @cpasswords.setter
    def cpassword(self, cpassword):
        # self.log.debug("setting cpassword to self. {0}".format(cpassword))
        if isinstance(cpassword, CPassword):
            self._cpasswords.append(cpassword)
        else:
            raise Exception("cpassword must be an instance of CPassword")

    # Not sure why i included this, doesn't seem to include a cpassword.
    # def _extract_from_defaults(self):
    #     """Extract passwords from defaults.xml"""
    #     self.log.warning("Extraction from defaults.xml not implemented.")
    #     # TODO: find format of this file and implement.

    def _get_properties_tags(self):
        """Parses self.local_file as XML, returns an iterator of Elements with
        tag "Properties".

        Returns:
            generator -- zero or more xml.etree.ElementTree.Element with tag
            "Properties" from parsed file.
        """
        tree = ET.parse(self.local_file)
        return tree.iterfind(".//Properties")

    def _extract_from_groups(self):
        """Extract cpassword and username from groups.xml

        Creates CPassword instance and passes to self.cpassword.
        """
        for properties in self._get_properties_tags():
            # self.log.debug("properties.get('cpassword') = {0}".format(
            #                properties.get("cpassword")))
            self.cpassword = CPassword(cpassword=properties.get("cpassword"),
                                       user_name=properties.get("userName"),
                                       file=self.remote_file)

    def _extract_from_services(self):
        """Extract cpassword and accountName from services.xml

        Creates CPassword instance and passes to self.cpassword.
        """
        for properties in self._get_properties_tags():
            self.cpassword = CPassword(cpassword=properties.get("cpassword"),
                                       user_name=properties.get("accountName"),
                                       file=self.remote_file)

    def _extract_from_scheduledtasks(self):
        """Extract cpassword and runAs from scheduledtasks.xml

        Creates CPassword instance and passes to self.cpassword.
        """
        for properties in self._get_properties_tags():
            self.cpassword = CPassword(cpassword=properties.get("cpassword"),
                                       user_name=properties.get("runAs"),
                                       file=self.remote_file)

    def _extract_from_datasources(self):
        """Extract cpassword and username from datasources.xml

        Creates CPassword instance and passes to self.cpassword.
        """
        for properties in self._get_properties_tags():
            self.cpassword = CPassword(cpassword=properties.get("cpassword"),
                                       user_name=properties.get("username"),
                                       file=self.remote_file)

    def _extract_from_printers(self):
        """Extract cpassword and username from printers.xml

        Creates CPassword instance and passes to self.cpassword.
        """
        for properties in self._get_properties_tags():
            self.cpassword = CPassword(cpassword=properties.get("cpassword"),
                                       user_name=properties.get("username"),
                                       file=self.remote_file)

    def _extract_from_drives(self):
        """Extract cpassword and username from drives.xml

        Creates CPassword instance and passes to self.cpassword.
        """
        for properties in self._get_properties_tags():
            self.cpassword = CPassword(cpassword=properties.get("cpassword"),
                                       user_name=properties.get("username"),
                                       file=self.remote_file)

    def _extract_password(self):
        """Select and call the appropriate parser based on self.local_filename.

        Raises:
            NotImplementedError: Any file that we do not have a parser causes
                                 NotImplementedError.
        """
        if self.local_file.lower().endswith("groups.xml"):
            self._extract_from_groups()
        elif self.local_file.lower().endswith("services.xml"):
            self._extract_from_services()
        elif self.local_file.lower().endswith("scheduledtasks.xml"):
            self._extract_from_scheduledtasks()
        elif self.local_file.lower().endswith("printers.xml"):
            self._extract_from_printers()
        elif self.local_file.lower().endswith("drives.xml"):
            self._extract_from_drives()
        elif self.local_file.lower().endswith("datasources.xml"):
            self._extract_from_datasources()
        # elif self.local_file.lower().endswith("defaults.xml"):
        #     self._extract_from_defaults()
        else:
            raise NotImplementedError(
                "Extracting from {0} not implemented.".format(
                    self.remote_file))

    def run(self):
        """Checks if self.password_in_file is True and calls
        self._extract_password() if so.

        Raises:
            ValueError: Any error checking the file for "password" results in
                        a ValueError.
            NotImplementedError: Files that contain "password" but cannot be
                                 parsed cause NotImplementedError.
        """
        self.log.debug("checking if 'password' in '{0}'".format(
            self.remote_file))
        try:
            result = self.password_in_file
        except Exception as e:
            raise ValueError(
                "Error checking if 'password' is in the '{0}'. {1}".format(
                    self.remote_file, e))
        else:
            if result is True:
                self.log.debug("extracting password from '{0}'...".format(
                    self.remote_file))
                self._extract_password()
            else:
                self.log.debug("'password' not in '{0}'.".format(
                    self.remote_file))
