'''
.       .1111...          | Title: authenticators.py
    .10000000000011.   .. | Author: Oliver Morton
 .00              000...  | Email: grimhacker@grimhacker.com
1                  01..   | Description:
                    ..    | SMBAuthenticator - uses either pywin32 or
                   ..     | subprocesses of linux utilities.
GrimHacker        ..      |
                 ..       |
grimhacker.com  ..        |
@grimhacker    ..         |
----------------------------------------------------------------------------
GPPPFinder - Group Policy Preference Password Finder
    Copyright (C) 2015  Oliver Morton

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
'''
import logging
import sys


class BaseSMBAuthenticator(object):
    """Base SMB authentication class."""
    def __init__(self, **kwargs):
        """Initialise base SMB authentication parameters.
        Note that this class cannot be used for authentication directly. It
        merely specificies the parameters and methods required for
        subclasses to implement to perform authentication with a consistent
        interface.


        Keyword Arguments:
            host {str} -- Remote host to connect to.
            user {str} -- Username to authenticate with.
            passwd {str} -- Password to authenticate with.
            domain {str} -- Domain to authenticate with.
            share {str} -- SMB share name to connect to. (default: "IPC$")

        Raises:
            ValueError: If host is not provided.
            TypeError: If host is not str.
            ValueError: If user is not provided.
            TypeError: If user is not str.
            ValueError: If passwd is not provided.
            TypeError: If passwd is not str.
            ValueError: If domain is not provided.
            TypeError: If domain is not a str.
            TypeError: If share is not str.
        """
        self.log = logging.getLogger(__name__)
        # Flag used to indicate whether deauthentication is allowed, set to
        # false when a session already existed which must be maintained.
        self._allow_deauth = True
        try:
            self._host = kwargs['host']
        except KeyError as e:
            raise ValueError("Target host required. {0}".format(e))
        else:
            if isinstance(self._host, str):
                pass
            else:
                raise TypeError("Target host must be a string.")

        try:
            self._user = kwargs['user']
        except KeyError:
            raise ValueError("Target user required.")
        else:
            if isinstance(self._user, str):
                pass
            else:
                raise TypeError("Target user must be a string.")
        try:
            self._passwd = kwargs['passwd']
        except KeyError:
            raise ValueError("Target passwd required.")
        else:
            if isinstance(self._passwd, str):
                pass
            else:
                raise TypeError("Target passwd must be a string.")
        try:
            self._domain = kwargs['domain']
        except KeyError:
            raise ValueError("Target domain required.")
        else:
            if isinstance(self._domain, str):
                pass
            else:
                raise TypeError("Target domain must be a string.")
        try:
            self._share = kwargs['share']
        except KeyError:
            self._share = "IPC$"
            self.log.debug("share not specified, using default: '{0}'".format(
                self._share))
        else:
            if isinstance(self._share, str):
                pass
            else:
                raise TypeError("share must be a string.")

        if self._host.startswith("\\\\"):
            self._host = self._host[2:]  # Strip \\ from start
        else:
            self._host = "{0}".format(self._host)

        # Construct target, results in: \\<host>\IPC$ by default.
        self._target = "\\\\{0}\\{1}".format(self._host, self._share)
        self.log.debug("target constructed as: '{0}'".format(self._target))
        self._local_device = None

    @property
    def user(self):
        """Read-only property for username to authenticate with.

        Returns:
            str -- username
        """
        return self._user

    @property
    def passwd(self):
        """Read-only property for password to authenticate with.

        Returns:
            str -- password
        """
        return self._passwd

    @property
    def domain(self):
        """Read-only property for domain to authenticate with.

        Returns:
            str -- domain name
        """
        return self._domain

    @property
    def target(self):
        """Read-only property for host/share to connect to.

        Returns:
            str -- \\\\HOST\\SHARE
        """
        return self._target

    @property
    def share(self):
        """Read-only property for share to connect to.

        Returns:
            str -- share name
        """
        return self._share

    @property
    def host(self):
        """Read-only property for remote host to connect to.

        Returns:
            str -- remote host
        """
        return self._host

    @property
    def local_device(self):
        """Local device name used to map remote filesystem locally.

        Arguments (setter):
            letter {str} -- local mount point for remote filesystem.

        Returns:
            str -- local device mapped to remote filesystem.
        """
        return self._local_device

    @local_device.setter
    def local_device(self, letter):
        self._local_device = letter

    def authenticate(self, auth_type="smb"):
        """Authenticate to the remote host.

        Keyword Arguments:
            auth_type {str} -- authentication protocol to use.
                               (default: {"smb"})

        Returns:
            bool -- True for success, False for failure (may also raise
                    exception)

        Raises:
            NotImplementedError: Base class does not implement functionality.
        """
        raise NotImplementedError(
            "Base class does not implement authentication functionality.")

    def deauthenticate(self):
        """Deauthenticate from the remote host.

        Returns:
            bool -- True for success, False for failure (may also raise
                    exception)

        Raises:
            NotImplementedError: Base class does not implement functionality.
        """
        raise NotImplementedError(
            "Base class does not implement deauthentication functionality.")

    def map(self):
        """Map/Mount the remote share to the local filesystem.

        Raises:
            NotImplementedError: Base class does not implement functionality.
        """
        raise NotImplementedError(
            "Base class does not implement drive mapping functionality.")

    @property
    def devices(self):
        """Return list of local devices that have shares mapped.

        Returns:
            list -- list of strings, each member is a known used mount
                    point/device.

        Raises:
            NotImplementedError: Base class does not implement functionality.
        """
        raise NotImplementedError(
            "Base class does not implement get devices functionality.")

    def _set_allow_deauth(self, value):
        """Set if deauthentication is allowed.
        If the connection was already established we do not want to
        disconnect.
        Note this intentionally not a setter as the allow_deauth property
        should only be configured internally.

        Arguments:
            value {bool} -- True to allow deauth, False to disallow.

        Raises:
            TypeError: If value is not bool.
        """
        if type(value) == bool:
            self._allow_deauth = value
        else:
            raise TypeError("allow_deauth must be True or False")

    @property
    def allow_deauth(self):
        """Read-only property to indicate if deauthentication/unmap/unmount
        is permitted.
        If the connection was already established we do not want to
        disconnect.

        Returns:
            bool -- Deauth allowed if True, disallowed if False.
        """
        return self._allow_deauth


# Declare SMBAuthenticator class for either Windows or *nix
if sys.platform == "win32":
    import win32net
    import win32api
    import pywintypes

    class SMBAuthenticator(BaseSMBAuthenticator):
        """Server Message Block Authenticator class using pywin32."""
        def __init__(self, **kwargs):
            """Initialise SMBAuthenticator.
            Extends BaseAuthentiactor.__init__()."""
            super(SMBAuthenticator, self).__init__(**kwargs)

        def _net_use_enum(self, server=None, level=1):
            """Enumerate all existing SMB connections.
            Depends on the win32.net NetUseEnum function.

            Keyword Arguments:
                server {str} -- Server to execute on (default: {None})
                level {int} -- Level of data required (default: {1})
                Refer to: http://timgolden.me.uk/pywin32-docs/win32net__NetUseEnum_meth.html

            Returns:
                list -- list of dict. dict are in PyUSE_INFO format dependant
                        on level
                        Refer to: http://timgolden.me.uk/pywin32-docs/PyUSE_INFO_.2a.html
            """
            all_connections = []
            # Flag used by the Windows API to indicate if there is more data.
            resume = 0
            while True:
                try:
                    # Use pywin32 to get a list of the connections.
                    (connections, total,
                     resume) = win32net.NetUseEnum(server, level, resume)
                except Exception as e:
                    # If there was an error, break out of the infinite loop by
                    # reraising the exception.
                    raise
                else:
                    all_connections = all_connections + connections
                    if not resume:
                        # There is no more information to retrieve using
                        # win32net.NetUseEnum so break from the loop.
                        break
            return all_connections

        @property
        def devices(self):
            """Return list of local devices that are in use.
            Depends on win32api.GetLogicalDriveStrings method.
            No distingtion between drive types.

            Returns:
                list -- list of str. Drive Letter and colon e.g. C:
            """
            try:
                devices = [
                    drive.strip("\\") for drive in
                    win32api.GetLogicalDriveStrings().split("\x00")
                ]
                # win32api.GetLogicalDriveStrings() returns a null byte
                # separated string of drives like this:
                # 'C:\\\x00D:\\\x00E:\\\x00F:\\\x00Z:\\\x00'
                # (Note it does not distinguish between drive types - which is
                # what we want at the moment, however we could use
                # GetDriveType to check the drive type if needed.)
                # The string is split into a list using the null byte as a
                # delimiter.
                # The resulting list is then iterated over to strip of the
                # trailing "\" (Note it needs escaping, hence "\\")
                # The resulting generator is converted to a list.
            except Exception as e:
                raise
            else:
                return devices

        def _already_authenticated(self):
            """Check if the system is already authenticated to the host.
            Return True if a connection to the remote host exists with status
            "Ok". If a connection exists with any other status deauthenticate
            it and return False. If no connection exists, return False.

            Returns:
                bool -- True if connection already established else False.
            """
            self.log.debug(
                "Checking for an established connection to {0}".format(
                    self._target))
            status = {
                0: 'Ok',
                1: 'Paused',
                2: 'Disconnected',
                3: 'Network Error',
                4: 'Connected',
                5: 'Reconnected'
            }
            # Flag used to indicate if there is already a session established.
            established = False
            try:
                # Use pywin32 to get a list of the connections.
                connections = self._net_use_enum()
            except Exception as e:
                self.log.warning(
                    "Could not retrieve list of existing connections. {0}".
                    format(e))
            else:
                for connection in connections:
                    # For each connection in the list check if it is connected
                    # to our target.
                    if connection['remote'] == self._target:
                        self.log.debug(
                            "connection to '{0}' exists with status '{1}'".
                            format(self._target, status[connection['status']]))
                        if connection['status'] == 0:
                            # If the status is 'OK', set the established flag
                            # to true to indicate that there is an existing
                            # connection that we can use.
                            established = True
                        else:
                            # If the status is not 'OK', deauthenticate
                            # because it is no use to us.
                            self.deauthenticate()
                            # Set the established flag to false to indicate
                            # that there is not an existing connect that we
                            # can use.
                            established = False
            return established

        def _net_use_add(self, server=None, level=2, data=None):
            """Establish SMB connection to remote host.
            Depends on win32net.NetUseAdd function.

            Keyword Arguments:
                server {str} -- The name of the server, or None.
                                (default: {None})
                level {int} -- The information level contained in the data
                               (default: {2})
                data {dict} -- A dictionary holding the share data in the
                               format of PyUSE_INFO_* (default: {None})
                Refer to: http://timgolden.me.uk/pywin32-docs/win32net__NetUseAdd_meth.html

            Raises:
                the: [description]
            """
            if data is None:
                data = {}
            try:
                # Use pywin32 to create a connection to the target.
                win32net.NetUseAdd(server, level, data)
            except Exception as e:
                raise  # reraise the exception.

        def authenticate(self, auth_type="smb"):
            """Authenticate to host using given credentials.
            Overrides BaseAuthenticator.authenticate().

            Checks for an established connection to the remote host and uses
            it if it exists (disallowing deauthentication). (Returns True)
            Attempts to connect to the remote host using the provided
            credentials. Returns True if successful otherwise False.

            Keyword Arguments:
                auth_type {str} -- authentication type to use
                                   (default: {"smb"})

            Returns:
                bool -- Returns True if successful otherwise False.
            """
            # Check if there is an existing connection to the target.
            if self._already_authenticated():
                self.log.info(
                    "Using established session to '{0}' and disallowing deauthentication."
                    .format(self._target))
                self._set_allow_deauth(False)
                # If there is an existing connection set the allow_deauth flag
                # to False so that the connection is not terminated later
                # (we didn't set it up so we won't tear it down.)

                # Return True to indicate we have a connection to the target.
                return True
            else:
                # Data structure required by NetUseAdd:
                data = {
                    'remote': self._target,
                    'local': "",
                    'password': self.passwd,
                    'username': self.user,
                    'domainname': self.domain,
                    'asg_type': 3
                }  # Note: asg_type:0 is a normal share 3 is an ipc$ share.
                try:
                    # Use pywin32 to create a connection to the target.
                    self._net_use_add(None, 2, data)
                except pywintypes.error as details:
                    self.log.critical(
                        "Failed to authenticate to '{0}'. ({1}, '{2}', '{3}')".
                        format(self._target, details[0], details[1],
                               details[2]))
                    # Clean up the failed authentication.
                    self.deauthenticate()

                    # Return flase to indicate that we did not successfully
                    # connect.
                    return False
                else:
                    self.log.debug("Authenticated to '{0}'".format(
                        self._target))
                    # Return true to indicate we successfully connected to
                    # the target.
                    return True

        def map(self):
            """Map/Mount the remote share so that it can be accessed on a
            local device.
            Windows systems only allow English Alphabet character device names
            of a single character followed by a colon.
            Iterate through the possibilities checking against the list of
            existing devices until a free drive letter is found.
            call net_use_add to map the remote share to the selected drive.

            Raises:
                ValueError: If no available drive letter can be identified.
            """
            # check if a local device has been set
            if self.local_device is None:
                letters = [
                    'Z:', 'Y:', 'X:', 'W:', 'V:', 'U:', 'T:', 'S:', 'R:', 'Q:',
                    'P:', 'O:', 'N:', 'M:', 'L:', 'K:', 'J:', 'I:', 'H:', 'G:',
                    'F:', 'E:', 'D:', 'C:', 'B:', 'A:'
                ]
                devices = self.devices
                for letter in letters:
                    if letter in devices:
                        # if drive letter is already in use, continue to the
                        # next drive letter
                        continue
                    else:
                        # drive letter is not in use, so we will use that.
                        self.local_device = letter
                        break
            if self.local_device is None:
                raise ValueError(
                    "Can't find a drive letter to mount on. Unmap some shares and try again."
                )
            else:
                data = {
                    'remote': self._target,
                    'local': self.local_device,
                    'password': self.passwd,
                    'username': self.user,
                    'domainname': self.domain,
                    'asg_type': 0
                }  # Note: asg_type:0 is a normal share 3 is an ipc$ share.
                try:
                    self._net_use_add(None, 2, data)
                except pywintypes.error as details:
                    self.log.critical(
                        "Failed to authenticate to '{0}'. ({1}, '{2}', '{3}')".
                        format(self._target, details[0], details[1],
                               details[2]))
                    self.deauthenticate(
                    )  # Clean up the failed authentication.
                except Exception as e:
                    self.log.critical(
                        "Failed to authenticate to '{0}'. ({1})".format(
                            self._target, e))

        def deauthenticate(self):
            """Deauthenticate from host.
            Overrides BaseAuthenticator.deauthenticate().
            Depends on win32net.NetUseDel function.

            Checks if allowed to deauthenticate.
            If not, log a warning and return True.
            Otherwise, use NetUseDel to delete the connection.
            Return True if successful, otherwise False.

            Returns:
                bool -- True if succesful (or not allowed),
                        otherwise False.
            """
            if self.allow_deauth:
                # We are allowed to tear down the connection.
                if self.local_device is not None:
                    mount = self.local_device
                else:
                    mount = self._target
                try:
                    # Use pywin32 to delete the connection to the target.
                    win32net.NetUseDel(None, mount)
                except pywintypes.error as details:
                    self.log.warning(
                        "Failed to deauthenticate from '{0}'. ({1}, '{2}', '{3}')"
                        .format(mount, details[0], details[1], details[2]))
                    return False
                else:
                    self.log.debug("Deauthenticated from '{0}'".format(mount))
                    if self.local_device is not None:
                        # Overwrite local device letter with None so that it
                        # isn't accidentally used after it has been
                        # deauthenticated.
                        self.local_device = None
                    return True
            else:
                self.log.warning("not allowed to deauthenticate.")
                return True

else:
    # print "Not running on Windows. Assuming *nix ."
    import os
    import string
    import random
    import tempfile

    from subprocess import CalledProcessError
    from subprocess import Popen, PIPE

    class Command(object):
        """Execute operating system command and get output.
        """
        def __init__(self, command=None, **kwargs):
            """Initialise Command class.

            Keyword Arguments:
                command {list} -- command and arguments to execute
                                  (default: {None})

            Raises:
                TypeError: If command is not list.
            """
            self.log = logging.getLogger(__name__)
            if command is not None and isinstance(command, list):
                self._command = command
            else:
                raise TypeError(
                    "command must be a list of executable and arguments.")
            self._returncode = None
            self._stdout = ""
            self._stderr = ""

        @property
        def returncode(self):
            """Read-only property. Return code of executed command.

            Returns:
                int -- return code
            """
            return self._returncode

        # @returncode.setter
        # def returncode(self, code):
        #     """Store returncode of executed command."""
        #     self._returncode = code

        @property
        def stdout(self):
            """Read-only property. Standard output of executed command.

            Returns:
                str -- stdout content
            """
            return self._stdout.decode()

        # @stdout.setter
        # def stdout(self, out):
        #     """Store stdout of executed command."""
        #     self._stdout = out

        @property
        def stderr(self):
            """Read-only property. Standard error of executed command.

            Returns:
                str -- stderr content
            """
            return self._stderr.decode()

        # @stderr.setter
        # def stderr(self, err):
        #     """Store stderr of executed command."""
        #     self._stderr = err

        @property
        def command(self):
            """Read-only property. Command and argument list to execute.

            Returns:
                list -- Command and argument list.
            """
            return self._command

        def execute(self):
            """Execute the provided command.
            Depends on subprocess.Popen

            Raises:
                OSError: Error executing command as subprocess.
                OSError: Error communicating with subprocess.
            """
            cmd = self.command
            self.log.debug("running: '{0}'".format(cmd))
            try:
                proc = Popen(cmd, stdout=PIPE, stderr=PIPE)
            except CalledProcessError as e:
                # self.log.error("{0}: {1}".format(e.errno, e.strerror))
                raise OSError(
                    "Failed to execute command as subprocess. {0}: {1}".format(
                        e.errno, e.strerr))
            except Exception as e:
                raise OSError(
                    "Failed to execute command as subprocess. {0}".format(e))
            else:
                try:
                    out, err = proc.communicate()
                except OSError as e:
                    raise Exception(
                        "Failed to communicate with subprocess. {0}".format(e))
                else:
                    if err is not None:
                        self._stderr = err
                    if out is not None:
                        self._stdout = out
                try:
                    self._returncode = proc.returncode
                except Exception as e:
                    self.log.warning(
                        "Failed to set subprocess return code to self. {0}".
                        format(e))

    class SMBAuthenticator(BaseSMBAuthenticator):
        """Server Message Block Authenticator class using Samba and Linux
        utilities."""
        def __init__(self, **kwargs):
            """Initialise SMBAuthenticator.
            Extends BaseAuthentiactor.__init__()."""
            super(SMBAuthenticator, self).__init__(**kwargs)
            # Create temporary directory we can use to mount remote shares
            # within.
            self._temp_dir = tempfile.mkdtemp()

        def deauthenticate(self):
            """Deauthenticate from the remote host.

            Check if a device has been mapped. If not return True since the
            authentication method does not establish a peristent connection.
            If a device is mapped, execute the umount command to dismount and
            delete the directory we created to mount on.

            Raises:
                OSError: If failed to execute umount command.

            Returns:
                bool -- True if successful, otherwise False.
            """
            if self.local_device is None:
                # Share isn't mapped and authenticate does not require
                # anything to be done to deauthenticate.
                return True
            else:
                try:
                    exe = Command(command=['umount', self.local_device])
                    exe.execute()
                except OSError as e:
                    self.log.warning(
                        "Failed to execute subprocess of umount. '{0}'".format(
                            e))
                    return False
                else:
                    if exe.returncode != 0:
                        self.log.warning(
                            "Failed to execute subprocess of umount. '{0}'".
                            format(exe.stderr))
                        # raise Exception("Failed to execute subprocess of umount. {0}".format(exe.stderr))
                        return False
                    else:
                        # successfully unmounted
                        self.log.debug("removing directory '{0}'".format(
                            self.local_device))
                        try:
                            os.rmdir(self.local_device)
                        except Exception as e:
                            self.log.warning(
                                "Failed to delete directory. '{0}'".format(e))
                            # raise Exception("Failed to delete directory. {0}".format(e))
                            return False
                        return True

        @property
        def devices(self):
            """Return list of local devices (file and directory names in directory).

            Returns:
                list -- list of file and directory name strings.
            """
            return os.listdir(self._temp_dir)

        def map(self):
            """Map/Mount the remote share to the local filesystem.
            Make a directory with a random name within our temp directory.
            It is expected that the temp directory is empty, however we check
            the random name we create is not in use just in case.
            Populate the local_device property with the created path.
            Execute the mount command to mount he remote share.
            """
            def _mkdir():
                """Make a random directory name within the temp directory.
                The temp directory should be empty, but check our random name
                isn't in use, just in case.
                Populate the loal_device property with the created path.

                Raises:
                    OSError: Failed to create directory.
                """
                while True:
                    # loop until created a name that isn't in the directory.
                    # create a directory name.
                    name = (''.join(
                        random.choice(string.ascii_lowercase)
                        for i in range(5)))  # create random 5 character string
                    # check the name is not already in use in the directory.
                    if name not in self.devices:
                        self.local_device = os.path.join(self._temp_dir, name)
                        break
                self.log.debug("creating directory '{0}'".format(
                    self.local_device))
                try:
                    # create a new directory to use as a mount point.
                    os.mkdir(self.local_device)
                except Exception as e:
                    raise OSError(
                        "Failed to create directory to use as mount point. {0}"
                        .format(e))

            def _mount():
                """Mount remote share onto local_device.
                Executes the `sudo mount -t cifs` command with credentials.

                Raises:
                    ValueError: Failed to create credential string.
                    OSError: Failed to execute mount command.
                    OSError: Mount command return code indicates failure.
                """
                try:
                    credentials = []
                    credentials.append("username={0}".format(self.user))
                    credentials.append("password={0}".format(self.passwd))
                    if self.domain != "":
                        credentials.append("domain={0}".format(self.domain))
                    creds_string = ",".join(credentials)
                except Exception as e:
                    raise ValueError(
                        "Failed to create credentials string. {0}".format(e))
                else:
                    try:
                        # mount the share on the directory.
                        exe = Command(command=[
                            'sudo', 'mount', '-t', 'cifs', self.target, '-o',
                            creds_string, self.local_device
                        ])
                        exe.execute()
                    except Exception as e:
                        raise OSError("Failed to mount. {0}".format(e))
                    else:
                        if exe.returncode != 0:
                            raise OSError(
                                "Failed to mount remote share as device. {0}".
                                format(exe.stderr))
                        else:
                            pass  # mounted share

            try:
                _mkdir()
            except Exception as e:
                raise
            else:
                try:
                    _mount()
                except Exception as e:
                    raise

        def authenticate(self, auth_type="smb"):
            """Authenticate to host using given credentials.
            Overrides BaseAuthenticator.authenticate().

            Executes smbclient to authenticate to the remote host and executes
            the `help` command to confirm a valid connection..

            Keyword Arguments:
                auth_type {str} -- authentication type to use (default: {"smb"})

            Returns:
                bool -- Returns True if successful otherwise False.
            """
            def _build_auth_cmd():
                """Build the command and argument list to excute subprocess
                using the parameters provided to the class init.

                Returns:
                    list -- command and argument list
                """
                cmd = [
                    'smbclient', '-g', '-W', '{0}'.format(self.domain),
                    '{0}'.format(self.target), '-U',
                    '{0}%{1}'.format(self.user, self.passwd), '-c', 'help'
                ]
                return cmd

            def _parse_auth_output(stdout, stderr, returncode):
                """Parse the stdout and stderr from the command output to
                determine if the authentication was successful or not.
                Return True if successful otherwise False.

                Arguments:
                    stdout {str} -- STDOUT from smbclient command.
                    stderr {str} -- STDERR from smbclient command.
                    returncode {int} -- returncode from smbclient command.

                Returns:
                    bool -- True if authentication successful, otherwise False
                """
                # self.log.debug("returncode: {0}".format(returncode))
                if returncode is not None:
                    if returncode == 0:
                        if "failed" in stdout:
                            # So far observed the following:
                            # Connection to 192.168.0.12 failed (Error NT_STATUS_IO_TIMEOUT)
                            # Connection to 192.168.0.20 failed (Error NT_STATUS_HOST_UNREACHABLE)
                            # session setup failed: NT_STATUS_LOGON_FAILURE
                            # tree connect failed: NT_STATUS_BAD_NETWORK_NAME
                            try:
                                reason = stdout.split("(")[1].split(")")[0]
                            except Exception as e:
                                reason = "Unknown"
                            finally:
                                self.log.warning(
                                    "Failed to authenticate. Reason: {0}".
                                    format(reason))
                            return False
                        elif "showconnect" in stdout or stderr.startswith(
                                "Domain=["):
                            # TODO: need to test this is correct.
                            # "Domain=[" used to be returned however that no
                            # longer seems to be the case or at least is not
                            # reliable cross-platform.
                            # "showconnect" is one of the commands in the
                            # `help` output so hopefullyshould be more robust.
                            return True
                    else:
                        self.log.warning(
                            "Failed to authenticate. Reason: returncode {0}".
                            format(returncode))
                        return False

            # Note: this function only tests for authentication because the
            # samba subprocesses need to handle authentication and command
            # as a single operation. No persistent session established.
            try:
                cmd = _build_auth_cmd()
            except Exception as e:
                self.log.warning("Failed to build command. {0}".format(e))
            else:
                try:
                    exe = Command(command=cmd)
                except Exception as e:
                    self.log.warning(
                        "Failed to initialise Command. {0}".format(e))
                else:
                    try:
                        exe.execute()
                    except Exception as e:
                        self.log.warning(
                            "Failed to execute command. {0}".format(e))
                    else:
                        try:
                            result = _parse_auth_output(
                                exe.stdout, exe.stderr, exe.returncode)
                            self.log.debug("stdout: {}".format(exe.stdout))
                            self.log.debug("stderr: {}".format(exe.stderr))
                            self.log.debug("returncode: {}".format(
                                exe.returncode))
                        except Exception as e:
                            self.log.warning(
                                "Failed to parse output. {0}".format(e))
                        else:
                            return result
