#!/usr/bin/env python3
'''
.       .1111...          | Title: gp3finder.py
    .10000000000011.   .. | Author: Oliver Morton
 .00              000...  | Email: grimhacker@grimhacker.com
1                  01..   | Description:
                    ..    | Group Policy preference password finder
                   ..     |
GrimHacker        ..      |
                 ..       |
grimhacker.com  ..        |
@grimhacker    ..         |
----------------------------------------------------------------------------
GPPPFinder - Group Policy Preference Password Finder
    Copyright (C) 2020  Oliver Morton

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
'''

import logging
import argparse
import getpass

from . import __version__
from .lib.mscrypto import MSCrypto
from .lib.finder import Finder
from .lib.reporter import Reporter

# $Source$

# TODO:
# report other files that appear to contain credentials (logon scripts, etc)?


class GPPPFinder(object):
    """Group Policy preference password finder.
       Find and decrypt passwords on share, decrypt given cpassword
       or encrypt given password.
    """
    def __init__(self, **kwargs):
        """Initialise GPPPFinder

        Operation of this class will vary greatly depending on the kwargs
        provided.
        There are three primiary modes: Encrypt, Decrypt, and Auto.
        Encrypt Mode - encrypt the provide plaintext and report the
                       ciphertext.
        Decrypt Mode - decrypt the provided ciphertext and report the
                       plaintext.
        Auto Mode - Search a file system for GPP files, decrypt and discovered
                    cpasswords, and report. There are two sub modes:
            Local - Search the local filesystem. The provided local_root is
                    used as the starting path for a recursive search.
            Remote - Search the SMB share on a remote host. The provided
                     remote_root is used as the starting path for a recursive
                     search.

        Keyword Arguments:
            decrypt {str} -- Ciphertext to decrypt (or None)
            encrypt {str} -- Plaintext to encrypt (or None)
            auto {bool} -- Run in Auto mode (True or False)
            remote {bool} -- Run in remote mode (True or False)
            hosts {list} -- List of str. Remote hosts to connect to.
            remote_root {str} -- Root path for search of remote share.
            share {str} -- SMB share name to connect to on remote host.
            user {str} -- Username to authenticate to remote share with.
                          Accepts DOMAIN\\USER, DOMAIN/USER, USER@DOMAIN.
            password {str} -- Password to authenticate to remote share with.
                              None will cause prompt for user input.
            local {bool} -- Run in local mode (True or False)
            local_root {str} -- Root path for search of local filesystem.
            outfile {str} -- Output filename.

        Raises:
            TypeError: If decrypt is not None or str
            TypeError: If encrypt is not None or str
            TypeError: If auto is not bool
            ValueError: If more than one of encrypt, decrypt, auto is True
            TypeError: If remote is not bool
            TypeError: If remote is True and hosts is not list of str
            TypeError: If remote is True and remote_root is not str
            TypeError: If remote is True and share is not str
            TypeError: If remote is True and user is not str
            TypeError: If remote is True and password is not str
            TypeError: If local is not bool
            TypeError: If local is True and local_root is not str
            TypeError: If outfile is not str
            ValueError: If auto and remote are True and not all of hosts,
                        share, user, password are provided.
            ValueError: If auto and local are True and local_root is not
                        provided.
            ValueError: If both or neither of remote and local are True.
        """
        self.log = logging.getLogger(__name__)

        self._ciphertext = kwargs.get('decrypt')
        if self._ciphertext is not None and not isinstance(
                self._ciphertext, str):
            raise TypeError("decrypt must be a string or None.")

        self._plaintext = kwargs.get('encrypt')
        if self._plaintext is not None and not isinstance(
                self._plaintext, str):
            raise TypeError("encrypt must be a string or None.")

        self._auto = kwargs.get('auto')
        if not isinstance(self._auto, bool):
            raise TypeError("auto must be a bool.")

        if not sum([bool(self._ciphertext),
                    bool(self._plaintext), self._auto
                    ]) == 1:  # bool inherits int so we can add them
            # check that only one mode has been specified.
            raise ValueError(
                "Must specify only one of encrypt, decrypt, auto.")

        self._remote = kwargs.get('remote', True)
        if not isinstance(self._remote, bool):
            raise TypeError("remote must be a bool.")
        elif self._remote:
            self._hosts = kwargs.get('hosts')
            if not (self._hosts and all(
                    [isinstance(host, str) for host in self._hosts])):
                raise TypeError("hosts must be a list of strings")

            self._remote_root = kwargs.get('remote_root', r"")
            if not isinstance(self._remote_root, str):
                raise TypeError("remote_root must be a string or None.")

            self._share = kwargs.get('share')
            if not (self._share and isinstance(self._share, str)):
                raise TypeError("share must be a string")

            self._domain = ""
            self._password = None
            self._user = None
            if kwargs.get('user') is None or not isinstance(
                    kwargs.get('user'), str):
                raise TypeError("user must be a string")
            elif kwargs.get('password') is None or not isinstance(
                    kwargs.get('password'), str):
                raise TypeError("password must be a string")
            else:
                self._get_creds(kwargs.get('user'), kwargs.get('password'))

        self._local = kwargs.get('local', False)
        if not isinstance(self._local, bool):
            raise TypeError("local must be a bool.")
        elif self._local:
            self._local_root = kwargs.get(
                'local_root', 'C:\\ProgramData\\Microsoft\\'
                'Group Policy\\History')
            if self._local_root is not None and not isinstance(
                    self._local_root, str):
                raise TypeError("local_root must be a string or None.")

        self._outfile = kwargs.get('outfile')
        if not isinstance(self._outfile, str):
            raise TypeError("outfile must be a string.")

        if self._auto:
            # if running in auto mode, check that the required information has
            # been provided.
            if self._remote and not (self._hosts and self._share
                                     and self._user is not None
                                     and self._password is not None):
                # Note domain isn't in this check because an empty string is
                # False but is a valid option.
                raise ValueError("Must specify host, share and credentials.")
            elif self._local and not self._local_root:
                raise ValueError("Must specify local_root.")
            elif (self._remote
                  and self._local) or not (self._remote or self._local):
                raise ValueError(
                    "Must specify either local or remote for auto running.")
            else:
                pass

    def _get_creds(self, user, password):
        """Parse credentials for authentication.

        Arguments:
            user {str} -- Username for authentication. Format:
                          DOMAIN\\USERNAME, DOMAIN/USERNAME, USERNAME@DOMAIN
            password {str} -- Password for authentication. If None
                              interactively prompt for password.

        Raises:
            ValueError: Any error setting the username.
            ValueError: Any error setting password.
        """
        try:
            if "\\" in user:  # domain\username
                self._domain, self._user = user.split("\\")
            elif "/" in user:  # domain/username:
                self._domain, self._user = user.split("/")
            elif "@" in user:  # username@domain
                self._user, self._domain = user.split("@")
            else:
                self._user = user
        except Exception as e:
            raise ValueError(
                "Error parsing user, should be 'user' or 'domain\\user' or "
                "'domain/user' or 'user@domain'.")
        if password is None:
            try:
                self._password = getpass.getpass()
            except Exception as e:
                raise ValueError("Error getting password. {0}".format(e))
        else:
            self._password = password

    @property
    def ciphertext(self):
        """Ciphertext to decrypt.
        Read-only property.

        Returns:
            str -- Ciphertext to decrypt.
        """
        return self._ciphertext

    @property
    def plaintext(self):
        """Plaintext to encrypt.
        Read-only property.

        Returns:
            str -- Plaintext to encrypt.
        """
        return self._plaintext

    @property
    def auto(self):
        """Auto mode configuration.
        Read-only property.

        Returns:
            bool -- Auto mode configuration.
        """
        return self._auto

    @property
    def local(self):
        """Local mode configuration.
        Read-only property.

        Returns:
            bool -- Local mode configuration.
        """
        return self._local

    @property
    def local_root(self):
        """Root path to use for local recursive search.
        Read-only property.

        Returns:
            str -- Local root path to start search from.
        """
        return self._local_root

    @property
    def remote(self):
        """Remote mode configuration.
        Read-only property.

        Returns:
            bool -- Remote mode configuration.
        """
        return self._remote

    @property
    def remote_root(self):
        """Root path to use for remote recursive search.
        Read-only property.

        Returns:
            str -- Remote root path to start search from.
        """
        return self._remote_root

    @property
    def outfile(self):
        """Output filename
        Read-only property.

        Returns:
            str -- Output filename.
        """
        return self._outfile

    @property
    def hosts(self):
        """Remote hosts to search.
        Read-only property.

        Returns:
            list -- List of strings, remote hosts to search.
        """
        return self._hosts

    @property
    def share(self):
        """Remote SMB share name to search.
        Read-only property.

        Returns:
            str -- Remote SMB share name to search.
        """
        return self._share

    @property
    def domain(self):
        """Domain to use for SMB authentication.
        Read-only property.

        Returns:
            str -- Domain name to use for SMB authentication.
        """
        return self._domain

    @property
    def user(self):
        """Username to use for SMB authentication.
        Read-only property.

        Returns:
            str -- Username to use for SMB Authentication.
        """
        return self._user

    @property
    def password(self):
        """Return password to authenticate with."""
        return self._password

    def _encrypt(self):
        """Encrypt configured plaintext using MSCrypto.

        Raises:
            Exception: Any error initialising MSCrypto.
            Exception: Any error using MSCrypto.encrypt()

        Returns:
            tuple -- (warning, ciphertext)
        """
        try:
            crypto = MSCrypto()
        except Exception as e:
            raise Exception("Error initalising MSCrypto. {0}".format(e))
        else:
            try:
                warning, ciphertext = crypto.encrypt(self.plaintext)
            except Exception as e:
                raise Exception("Error running encryption. {0}".format(e))
            else:
                return warning, ciphertext

    def _decrypt(self):
        """Decrypt the configured ciphertext using MSCrypto.

        Raises:
            Exception: Any error initialising MSCrypto
            Exception: Any error running MSCrypto.decrypt()

        Returns:
            str -- plaintext decryption of ciphertext.
        """
        try:
            crypto = MSCrypto()
        except Exception as e:
            raise Exception("Error initalising MSCrypto. {0}".format(e))
        else:
            try:
                plaintext = crypto.decrypt(self.ciphertext)
            except Exception as e:
                raise Exception("Error running decryption. {0}".format(e))
            else:
                return plaintext

    def _autopwn(self):
        """Auto mode.
        Authenticate and map remote share if required.
        Locate and decrypt GPP files using Finder.
        Report findings using Reporter.
        """
        def run_finder(finder):
            """Run Finder instance.

            Arguments:
                finder {Finder} -- Initialised instance of Finder to run.

            Raises:
                Exception: Any error running Finder, retrieving cpasswords,
                           or retrieving password files.

            Returns:
                tuple -- cpasswords {list}, password_files {list} from Finder.
            """
            self.log.debug("running finder...")
            try:
                finder.run()
            except Exception as e:
                raise Exception("Error running finder. {0}".format(e))
            else:
                self.log.debug(
                    "getting cpasswords and password_files from finder...")
                cpasswords = []
                password_files = []
                try:
                    cpasswords = finder.cpasswords
                except Exception as e:
                    self.log.warning("Error getting cpasswords. {0}".format(e))
                try:
                    password_files = finder.password_files
                except Exception as e:
                    self.log.warning("Error getting password files.")
                return cpasswords, password_files

        cpasswords = []
        password_files = []
        if self.remote:
            for host in self.hosts:  # TODO: Threading?
                self.log.info("Working on: {0}".format(host))
                try:
                    self.log.debug("initialising finder for remote search...")
                    finder = Finder(host=host,
                                    share=self.share,
                                    remote_root=self.remote_root,
                                    domain=self.domain,
                                    user=self.user,
                                    password=self.password)
                except Exception as e:
                    self.log.warning(
                        "Error initialising finder for remote search. {0}".
                        format(e))
                    continue
                else:
                    try:
                        new_cpasswords, new_password_files = run_finder(finder)
                        cpasswords += new_cpasswords
                        password_files += new_password_files
                    except Exception as e:
                        self.log.warning("Error running finder. {}".format(e))
        elif self.local:
            try:
                self.log.debug("initialising finder for local search...")
                finder = Finder(local_root=self.local_root)
            except Exception as e:
                raise Exception(
                    "Error initialising finder for local search. {0}".format(
                        e))
            else:
                cpasswords, password_files = run_finder(finder)
        else:
            raise Exception("Must specify either local or remote autopwning.")

        self.log.debug("reporting...")
        try:
            report = Reporter(cpasswords=cpasswords,
                              password_files=password_files,
                              output_file=self.outfile)
            report.run()
        except Exception as e:
            raise Exception("Error reporting results. {0}".format(e))
        else:
            self.log.info("Reporting complete. Check: '{0}'".format(
                self.outfile))

    def run(self):
        """Run GPPPFinder.
        Operation varies depending on configuration initialised with.
        Refer to __init__ documentation.
        """
        if self.ciphertext is not None:
            try:
                decrypted = self._decrypt()
            except Exception as e:
                self.log.critical("Failed to decrypt. {0}".format(e))
            else:
                decrypted_len = len(decrypted)
                self.log.info("Decrypted password is {} characters.".format(
                    decrypted_len))
                self.log.info("-" * decrypted_len)
                self.log.info(decrypted)
                self.log.info("-" * decrypted_len)
                with open(self.outfile, "w") as f:
                    f.write("cpassword:password\n")
                    f.write("{}:{}".format(self.ciphertext, decrypted))
        elif self.plaintext is not None:
            try:
                warning, encrypted = self._encrypt()
            except Exception as e:
                self.log.critical("Failed to encrypt. {0}".format(e))
            else:
                print(warning)
                print(encrypted)
        elif self.auto is not None:
            try:
                self._autopwn()
            except Exception as e:
                self.log.critical(
                    "Failed to auto get and decrypt passwords. {0}".format(e))
            else:
                pass
        else:
            self.log.critical(
                "Nothing to do. Specify encrypt decrypt or auto.")


def print_version():
    """Print command line version banner."""
    print("""

.       .1111...          | Title: gp3finder.py {0}
    .10000000000011.   .. | Author: Oliver Morton
 .00              000...  | Email: grimhacker@grimhacker.com
1                  01..   | Description:
                    ..    | Group Policy preference password finder
                   ..     | Find and decrypt passwords on share or decrypt
GrimHacker        ..      | given cpassword or encrypt given password.
                 ..       | Requires: PyCrypto
grimhacker.com  ..        |           PyWin32 on Windows
@grimhacker    ..         |           Permissions to mount/map shares
----------------------------------------------------------------------------
""".format(__version__))


def setup_logging(verbose=True, log_file=None):
    """Setup logging configuration.

    Keyword Arguments:
        verbose {bool} -- Configure DEBUG logging if True. (default: {True})
        log_file {str} -- filename to log to. None disables log file.
                          (default: {None})
    """
    if log_file is not None:
        logging.basicConfig(
            level=logging.DEBUG,
            format="%(asctime)s: %(levelname)s: %(module)s: %(message)s",
            filename=log_file,
            filemode='w')
        console_handler = logging.StreamHandler()
        formatter = logging.Formatter("%(levelname)s: %(module)s: %(message)s")
        console_handler.setFormatter(formatter)
        if verbose:
            console_handler.setLevel(logging.DEBUG)
        else:
            console_handler.setLevel(logging.INFO)
        logging.getLogger().addHandler(console_handler)
    else:
        if verbose:
            level = logging.DEBUG
        else:
            level = logging.INFO
        logging.basicConfig(level=level,
                            format="%(levelname)s: %(module)s: %(message)s")


def main():
    """GP3Finder command line argument parser."""
    print("""
        Group Policy Preference Password Finder (GP3Finder) {0}
        Copyright (C) 2020  Oliver Morton
        This program comes with ABSOLUTELY NO WARRANTY.
        This is free software, and you are welcome to redistribute it
        under certain conditions. See GPLv2 License.
""".format(__version__))

    parser = argparse.ArgumentParser(
        description="Group Policy Preference Password Finder.")
    mutex_group = parser.add_mutually_exclusive_group()
    mutex_group.add_argument("-D", "--decrypt", help="cpassword to decrypt.")
    mutex_group.add_argument("-E", "--encrypt", help="plaintext to encrypt.")
    mutex_group.add_argument(
        "-A",
        "--auto",
        help="Check for and attempt to decrypt passwords on share.",
        action="store_true")
    parser.add_argument("-l",
                        "--local",
                        help="Search a local path INSTEAD of remote share.",
                        action="store_true")
    parser.add_argument(
        "-lr",
        "--local-root",
        help="Root of local search",
        default="c:\\ProgramData\\Microsoft\\Group Policy\\History\\")
    parser.add_argument(
        "-rr",
        "--remote-root",
        help="Root of remote search. You probably want this to be "
        "'ProgramData\\Microsoft\\Group Policy\\History' if share is C$",
        default="")
    parser.add_argument("-o",
                        "--outfile",
                        help="Output filename.",
                        default="gp3finder.out")
    targets = parser.add_mutually_exclusive_group()
    targets.add_argument(
        "-t",
        "--targets",
        help="Targets to authenticate to (usually a domain controller).",
        dest="hosts",
        nargs="+")
    targets.add_argument("-f", "--file", help="File of targets, one per line.")
    parser.add_argument("-v",
                        "--verbose",
                        help="Debug logging",
                        action="store_true")
    parser.add_argument("-V",
                        "--version",
                        help="Print version banner",
                        action="store_true")
    auth_group = parser.add_argument_group("Authentication")
    auth_group.add_argument("-u",
                            "--user",
                            help="Username for authentication domain\\user .")
    auth_group.add_argument(
        "-p",
        "--password",
        help="Password to authenticate with, omit to be prompted.")
    auth_group.add_argument("-s",
                            "--share",
                            help="Share to authenticate to.",
                            default="sysvol")

    args = parser.parse_args()

    args_dict = vars(args)

    input_file = args_dict.pop("file")
    hosts = []
    if input_file:
        with open(input_file, "r") as f:
            for line in f:
                hosts.append(line.strip())
        # Currently don't need this since --target and --file are mutually
        # exclusive, this may change in the future.
        # if args_dict['hosts'] is None:
        #     args_dict['hosts'] = []
        # args_dict['hosts'] += hosts
        args_dict['hosts'] = hosts

    if args_dict["local"] or args_dict["decrypt"] or args_dict["encrypt"]:
        args_dict['remote'] = False
    else:
        args_dict['remote'] = True

    setup_logging(args_dict.pop('verbose'))

    if args_dict.pop('version'):
        print_version()
        exit()

    if not sum([bool(args.encrypt),
                bool(args.decrypt), args.auto
                ]) == 1:  # bool inherits int, so we can add them together.
        print("Specify: encrypt, decrypt or auto.")
        parser.print_usage()
    else:
        try:
            gpppfinder = GPPPFinder(**args_dict)
        except Exception as e:
            logging.critical("Failed to initialise GPPPFinder. {0}".format(e))
        else:
            try:
                gpppfinder.run()
            except Exception as e:
                logging.critical("Failed to run GPPPFinder. {0}".format(e))


if __name__ == '__main__':
    main()
